---
title: Direito Natural ou A Ciência da Justiça
subititle: Um tratado sobre o direito natural, a justiça natural, a liberdade natural e a sociedade natural. Uma demonstração de que toda e qualquer legislação é Uma absurdidade, uma usurpação e um crime.
creator:
- role: author
  text: Lysander Spooner
- role: trl
  text: Tradução - Erick Vasconcelos
- role: rev
  text: Revisão - Marcelo Werlang de Assis
date: 1882
language: pt-BR
cover-image: cover/direito-natural.jpg
...
# Direito Natural ou A Ciência da Justiça
 Um tratado sobre o direito natural, a justiça natural, a liberdade natural e a sociedade natural. Uma demonstração de que toda e qualquer legislação é Uma absurdidade, uma usurpação e um crime.

- **Autor:** Lysander Spooner
- **Título original:** Natural Law or The Science of Justice
- **Ano em que foi publicado pela primeira vez:** 1882
- **Tradução:** Erick Vasconcelos
- **Revisão:** Marcelo Werlang de Assis

>Se existe o princípio da justiça, ele é, necessariamente, um princípio natural; e, como tal, é um assunto da ciência, a ser aprendido e aplicado como qualquer outro de outra ciência.
>**Lysander Spooner**

## Capítulo I — A Ciência da Justiça

### Seção I

A ciência do meu e do teu — a ciência da justiça — é a ciência de todos os direitos humanos; de todos os direitos do homem à sua pessoa e à sua propriedade; de todos os seus direitos à vida, à liberdade e à busca da felicidade.

É a ciência que, sozinha, pode dizer a qualquer homem o que ele pode e não pode fazer; o que ele pode e não pode ter; o que ele pode e não pode dizer sem infringir os direitos das outras pessoas.

É a ciência da paz — e a única ciência da paz: visto que é a ciência que, sozinha, pode nos dizer sob quais condições os homens podem ou devem viver em paz uns com os outros.

As condições são simplesmente estas: a saber, primeiro, que cada homem deve fazer em relação a todos os outros tudo aquilo que a justiça requeira que ele faça; por exemplo, ele deve pagar as suas dívidas; devolver a propriedade pega emprestada ou roubada do dono; e reparar qualquer dano que tenha ocasionado à pessoa ou à propriedade dos outros.

A segunda condição é a de que cada homem deve se abster de fazer a todos os outros o que quer que a justiça requeira que ele se abstenha de fazer; por exemplo, ele deve se abster de roubar, furtar, incendiar, assassinar, bem como de praticar qualquer outro crime contra a pessoa ou a propriedade dos outros.

Uma vez que essas condições sejam satisfeitas, os homens estão em paz e devem permanecer em paz uns com os outros. No entanto, quando qualquer dessas condições é violada, os homens entram em guerra entre si. E eles devem necessariamente permanecer em guerra até que a justiça seja restabelecida.

Através dos tempos, pelo que a história nos informa, onde quer que os homens tenham tentado viver em paz, tanto os instintos naturais quanto a sabedoria coletiva da raça humana reconheceram e prescreveram, como condição indispensável, a obediência a esta única obrigação universal: a saber, a de que cada um deve viver honestamente em relação aos outros.

A antiga máxima que resume o dever do homem para com os seus semelhantes é simplesmente esta: “Viver honestamente; não lesar ninguém; dar a cada um o que lhe é devido.” ^[Lembremos o famoso escrito do célebre jurista romano Ulpiano na sua obra Digesta (ou Pandectae): “Iuris praecepta sunt haec: honeste vivere, alterum non laedere, suum cuique tribuere.” - “Os preceitos da justiça são estes: viver honestamente; não lesar outrem; dar a cada um aquilo que é seu.”  (Nota do Revisor — N. do R.)]

Toda essa máxima pode ser realmente expressada nas palavras viver honestamente,já que viver honestamente significa não lesar ninguém e dar a cada um o que lhe é devido.

### Seção II

O homem, sem dúvida, tem muitos outros deveres morais para com os seus semelhantes, tais como alimentar os famintos, vestir os desagasalhados, abrigar os sem-teto, cuidar dos doentes, proteger os indefesos, prover assistência aos fracos e esclarecer os ignorantes. Mas essas são simplesmente obrigações morais, das quais, em cada caso particular, cada homem deve ser o seu próprio juiz quanto a se, como e em que medida ele pode ou deve exercê-las. Contudo, quanto à sua obrigação legal (jurídica) — isto é, quanto ao dever de viver honestamente em relação aos demais —, os outros não apenas podem julgar, mas também, para a própria proteção deles, devem julgar. E, se necessário, podem legitimamente compeli-lo a exercê-la. Eles podem fazer isso agindo sozinhos ou em concerto. Podem fazê-lo de imediato, quando surgir a necessidade; ou deliberada e sistematicamente, caso preferirem e caso a conveniência admitir.

### Seção III

Embora seja do direito de qualquer um e de todos — de qualquer único homem ou conjunto de homens, não menos um do que o outro — repelir a injustiça e compelir ao exercício da justiça, para si mesmos e para todos aqueles que sejam injustiçados, a fim de que se evitem erros resultantes da pressa e da paixão e a fim de que todos possam descansar com a certeza de que serão protegidos sem apelo à força, é evidentemente desejável que os homens se associem, de forma livre e voluntária, para a manutenção da justiça entre si e para a proteção mútua contra a força dos criminosos. É também altamente desejável que acordem entre si algum plano ou sistema de procedimentos judiciais, o qual, no processo das causas, deve assegurar cautela, deliberação, exaustiva investigação e, tanto quanto possível, liberdade de toda influência além do desejo de fazer justiça.

Entretanto, tais associações só podem ser legítimas e desejáveis enquanto permanecerem puramente voluntárias. Nenhum homem pode legitimamente ser coagido a sustentar — ou a se juntar a — uma associação contra a sua vontade. O seu próprio interesse, o seu próprio julgamento e a sua própria consciência — somente esses — devem determinar se ele vai ou não se juntar a esta ou àquela associação; ou mesmo se ele vai ou não se juntar a qualquer uma. Se ele escolher depender, para a proteção dos seus direitos, só de si mesmo e da assistência voluntária que outras pessoas venham a livremente lhe oferecer quando a necessidade surgir, o homem tem total direito de fazer isso. E esse caminho seria razoavelmente seguro de ser trilhado se ele manifestasse a comum prontidão da humanidade, em casos parecidos, de ir em assistência e em defesa das pessoas prejudicadas e se ele mesmo “vivesse honestamente, não lesasse ninguém e desse a cada um o que lhe seja devido”. Pois esse homem é razoavelmente certo de sempre oferecer o suficiente aos amigos e aos defensores em caso de necessidade, mesmo que tenha ou não se juntado a qualquer associação.

Certamente, não se pode requerer que nenhum homem sustente — ou se junte a — uma associação cuja proteção ele não deseja. Nem se pode esperar que nenhum homem sustente — ou se junte a — qualquer associação cujos planos ou métodos de procedimento ele não aprova, ainda que seja provável que ela atinja o seu professado propósito de assegurar a justiça e de, ao mesmo tempo, evitar a injustiça. Seria absurdo sustentar — ou juntar-se a — uma associação que fosse, em sua opinião, ineficiente. Sustentar — ou juntar-se a — uma associação que praticasse ela própria injustiças seria criminoso. Ele precisa, portanto, possuir a mesma liberdade de se associar ou não para esse propósito, como para qualquer outro, de acordo com o que os seus próprios interesses, o seu próprio juízo ou a sua própria consciência ditarem.

Uma associação para proteção mútua contra a injustiça é como uma associação para proteção mútua contra incêndios ou naufrágios. E não há mais legitimidade ou razão em compelir qualquer homem a sustentar — ou a se juntar a — uma dessas associações contra a sua vontade, o seu julgamento ou a sua consciência do que há em compeli-lo a sustentar — ou a se juntar a — qualquer outra cujos benefícios (caso ofereça algum) ele não deseja ou cujos propósitos e métodos ele não aprova.

### Seção IV

Nenhuma objeção pode ser feita a essas associações voluntárias com base no argumento de que elas não possuiriam o conhecimento da justiça — como uma ciência — que seria necessário para capacitá-las a assegurar a justiça e evitar a injustiça. A honestidade, a justiça, o direito natural, trata-se de um assunto muito evidente e simples, facilmente entendido pelas mentes comuns. Aqueles que desejem saber qual é a justiça, em qualquer caso particular, raramente têm de ir muito longe para encontrá-la. Ela é verdadeira; e ela, como qualquer outra ciência, deve ser aprendida. Mas é também verdadeiro que ela é facilmente aprendida. Embora seja tanto ilimitada em suas aplicações quanto sejam as infinitas relações e condutas do homem para com os outros, ela é, no entanto, constituída de alguns princípios elementares simples, dos quais a verdade pode ser quase que intuitivamente percebida por toda mente comum. E quase todos os homens têm a mesma percepção do que constitui a justiça ou do que a justiça requer quando eles entendem de forma similar os fatos dos quais as suas inferências devem ser derivadas.

Os homens, vivendo em contato uns com os outros e tendo relações em conjunto, em grande medida não podem evitar aprender a lei natural, ainda que quisessem. As relações dos homens com os outros homens; as suas posses separadas e os seus desejos individuais; a disposição de cada um de exigir — e nisso insistir — o que ele acredita que lhe seja devido e de resistir — e delas ressentir-se — a todas as invasões do que ele acredita serem os seus direitos; tudo isso, continuamente, força as suas mentes a perguntarem: “Este ato é justo ou injusto?”, “Esta coisa é minha ou dele?”. E essas são as perguntas do direito natural, questões que, em relação à grande maioria dos casos, são respondidas de forma semelhante pela mente humana em todo lugar. ^[Sir William Jones, um juiz inglês na Índia e um dos mais instruídos juízes que já viveram, familiarizado tanto com o direito asiático quanto com o direito europeu, declara: “É prazeroso notar a similaridade — ou, melhor dizendo, a identidade — daquelas conclusões que a razão pura, desenviesada, em todas as eras e em todas as nações, raramente deixa de derivar naqueles processos jurídicos que não estão agrilhoados e algemados pelas instituições positivas.” — Jones on Bailments, 133.  
Ele quer dizer que, quando nenhuma lei foi feita em violação à justiça, os tribunais, “em todas as eras e em todas as nações”, “raramente” deixaram de concordar sobre o que é a justiça.]

As crianças aprendem os princípios fundamentais do direito natural desde muito cedo. Assim, elas rapidamente entendem que uma criança não pode, sem justa causa, agredir ou ferir de outra forma outra criança; que uma criança não pode assumir qualquer controle arbitrário ou dominação sobre outra; que uma criança não pode, por força ou fraude, obter aquilo que pertence a outra; que, se uma criança infligir qualquer uma dessas coisas a outra, não apenas a criança prejudicada tem o direito de resistir e, se necessário, de punir quem a prejudicou e compeli-la a restituir o dano, mas também as outras crianças, bem como todas as outras pessoas, têm o direito — e o dever moral — de assistir a parte prejudicada na defesa dos seus direitos e na reparação dos seus danos. Esses são os princípios fundamentais do direito natural, o qual governa as mais importantes relações humanas. E, contudo, as crianças os aprendem mais cedo do que quando aprendem que três mais três são seis ou que cinco e cinco são dez. As brincadeiras infantis não podem ser executadas sem uma constante preocupação com eles; e é igualmente impossível que as pessoas de qualquer idade vivam juntas em paz em quaisquer outras condições.

Não seria extravagância dizer que, na maioria dos casos — se não em todos —, a humanidade em geral, os jovens e os velhos, aprende esse direito natural muito antes de aprender os significados das palavras com o qual o descrevemos. Na verdade, seria impossível fazer as pessoas entenderem o real significado das palavras se elas não entendessem a natureza das coisas em si mesmas. Fazê-las entender os significados das palavras justiça e injustiça antes de saberem a natureza das coisas em si mesmas seria tão impossível quanto fazê-las entender o significado das palavras quente e frio, seco e molhado, luz e escuridão, branco e preto, um e dois antes de conhecerem a natureza das coisas em si mesmas. O homem, necessariamente, precisa conhecer os sentimentos e as ideias não menos do que as coisas materiais antes que possa conhecer os significados das palavras através das quais os descrevemos.

## Capítulo II — A Ciência da Justiça (continuação)

### Seção I

Se a justiça não for um princípio natural, ela não é princípio algum. Se não for um princípio natural, não existe justiça alguma. Se não for um princípio natural, tudo aquilo que os homens já escreveram ou disseram sobre ela, desde tempos imemoriais, foi dito e escrito sobre algo que não tinha existência. Se ela não for um princípio natural, todos os apelos por justiça que já foram ouvidos, bem como todas as lutas por justiça que já foram testemunhadas, foram apelos e lutas por uma mera fantasia, por um capricho da imaginação — e não pela realidade.

Se a justiça não for um princípio natural, não existe nada que se possa chamar de injustiça; e todos os crimes dos quais o mundo foi palco não foram de fato crimes, mas apenas eventos, como o cair da chuva ou o pôr-do-sol; eventos dos quais as vítimas não tinham mais direito de reclamar do que teriam do passar das estações ou do crescer da vegetação.

Se a justiça não for um princípio natural, os governos (assim chamados) não têm mais direito ou motivo para tomar conhecimento dela — ou para fingir dela tomar conhecimento — do que têm para tomar conhecimento — ou para fingir ou professar tomar conhecimento — de qualquer outra não entidade; todos os seus atos de estabelecer, manter ou recompensar a justiça são simplesmente bobagens de tolos ou fraudes de impostores.

Entretanto, se a justiça é um princípio natural, ela, então, é necessariamente imutável; e ela não pode mais ser modificada — por nenhum poder inferior ao que a estabeleceu — do que podem sê-las a lei da gravidade, as leis da eletricidade, as leis da matemática ou quaisquer outras leis (princípios) naturais; e todas as tentativas ou suposições, por parte de qualquer homem ou grupo de homens — chamando-se pelo nome de governo ou por qualquer outro — de estabelecer os seus próprios comandos, caprichos, vontades ou discrições no lugar da justiça, como regra de conduta para qualquer ser humano, são uma absurdidade, uma usurpação e uma tirania tão grandes quanto seriam as tentativas de estabelecer os seus próprios comandos, caprichos, vontades ou discrições no lugar de quaisquer das leis físicas, mentais e morais do universo.

### Seção II

Se existe o princípio da justiça, ele é, necessariamente, um princípio natural; e, como tal, é um assunto da ciência, a ser aprendido e aplicado como qualquer outro de outra ciência. E falar em lhe adicionar ou retirar através da legislação é tão absurdo, falso e ridículo quanto seria falar em adicionar à matemática, à química ou a qualquer outra ciência — ou em delas tirar — através da legislação.

### Seção III

Se existe na natureza o princípio da justiça, nada pode ser adicionado — ou dela tirado — à sua suprema autoridade por toda e qualquer legislação que a humanidade inteira é capaz de fazer. E todas as tentativas da raça humana ou de qualquer porção sua de adicionar — ou dela retirar — à suprema autoridade da justiça, em qualquer caso, não têm obrigatoriedade alguma sobre qualquer ser humano.

### Seção IV

Se existe o princípio da justiça ou do direito natural, ele, então, é o princípio ou a lei que nos diz (1) quais direitos foram dados a todo ser humano na Terra em seu nascimento; que nos diz (2) quais direitos, portanto, lhe são inerentes na condição de ser humano e necessariamente mantidos com ele durante a sua vida; direitos esses que, embora possam ser desrespeitados, não podem desaparecer — nem ser extintos, aniquilados, separados ou eliminados — da sua natureza como ser humano; que nos diz (3) que o homem não pode ser destituído das autoridades ou obrigatoriedades a eles inerentes.

Por outro lado, se não existir um princípio como o da justiça (do direito natural), então todo ser humano veio à Terra totalmente destituído de direitos; e, vindo ao mundo destituído de direitos, ele deve para sempre permanecer dessa forma. Pois, se ninguém traz nenhum direito consigo ao mundo, claramente ninguém pode ter direito algum ou dar qualquer direito para os outros. E a consequência seria que a humanidade nunca teria quaisquer direitos; e falar de coisas como os direitos dela seria falar de coisas que nunca existiram e jamais terão qualquer existência.

### Seção V

Se existe esse princípio da justiça, então ele é, necessariamente, a suprema — e, em virtude disso, a única e universal — lei para todas as questões às quais é naturalmente aplicável. E, consequentemente, toda e qualquer legislação humana é, simplesmente e sempre, uma presunção de autoridade e domínio onde nenhum direito de autoridade ou domínio existe. É, portanto, sempre uma intrusão, uma absurdidade, uma usurpação e um crime.

Por outro lado, se não existir esse princípio natural da justiça, não pode existir desonestidade, e nenhum possível ato de força ou de fraude cometido por um homem contra a pessoa ou a propriedade de outro pode ser chamado de injusto ou desonesto; não é possível que dele se reclame ou que ele seja proibido e punido como tal. Em suma, se não houver esse princípio da justiça, não pode haver atos como crimes; e todos os atos dos governos (assim chamados) que existem total ou parcialmente para punir ou evitar crimes são atos que existem para a punição ou o impedimento daquilo que nunca existiu ou que não pode existir. Esses atos são, assim, confissões de que, em relação aos crimes, os governos não podem existir; de que não há nada para eles fazerem; de que não há nada que eles possam fazer. Eles são confissões de que os governos existem para a punição e o impedimento de atos que são, por natureza, meras impossibilidades.

### Seção VI

Se existem na natureza o princípio da justiça, o princípio da honestidade, os princípios que nós descrevemos pelas palavras meu e teu, tais princípios como os dos direitos naturais do homem à pessoa e à propriedade, então nós temos uma lei imutável e universal; uma lei que nós podemos aprender, assim como podemos aprender qualquer outra ciência; uma lei que nos diz o que é justo e o que é injusto, o que é honesto e o que é desonesto, que coisas são minhas e que coisas são tuas, quais são os meus direitos à pessoa e à propriedade e quais são os teus direitos à pessoa e à propriedade, bem como onde se encontra a fronteira entre os meus direitos à pessoa e à propriedade e os teus direitos à pessoa e à propriedade. E essa lei é a lei suprema, e ela é a mesma em todo o mundo, em todos os tempos, para todas as pessoas; e será a mesma lei suprema e única, em todos os tempos, para todas as pessoas, enquanto o homem viver sobre a Terra.

No entanto, se, por outro lado, não existirem na natureza o princípio da justiça, o princípio da honestidade, os princípios dos direitos naturais do homem à pessoa e à propriedade, então as palavras justiça e injustiça, honestidade e desonestidade, as palavras meu e teu, as palavras que significam que uma coisa é a propriedade de um homem e que outra coisa é a propriedade de outro homem, as palavras que são usadas para descrever os direitos naturais à pessoa e à propriedade, as palavras que são usadas para descrever prejuízos e crimes deveriam ser abolidas dos idiomas humanos em razão de não possuírem significado; e deveria ser declarado que, de uma vez por todas, a maior força e a maior fraude, por ora, são as únicas e supremas leis que governam as relações dos homens uns com os outros; e que, doravante, todas as pessoas e combinações de pessoas — tanto aquelas que se chamam de governos como todas as outras — devem ser deixadas livres para exercer em relação aos outros toda força e toda fraude de que são capazes.

### Seção VII

Se não existir o princípio da justiça, não pode haver ciência do governo; e toda a rapacidade e toda a violência através das quais, em todas as eras e em todas as nações, uns poucos criminosos confederados conquistaram o domínio sobre o resto da humanidade, reduziram-no à pobreza e à escravidão e estabeleceram o que eles chamaram de governos para mantê-lo em sujeição foram exemplos tão legítimos de governo como qualquer outro que o mundo jamais verá.

### Seção VIII

Se existe esse princípio da justiça, ele é necessariamente o único princípio político que já existiu e existirá. Todos os outros chamados princípios políticos, os quais o homem tem o hábito de inventar, não são princípios de forma alguma. São apenas vaidades de tolos, que imaginam que descobriram algo melhor do que a verdade, a justiça e a lei universal; ou são apenas instrumentos e pretensões a que homens egoístas e desonestos recorrem para obter fama, poder e dinheiro.

### Seção IX

Se não existir, na natureza, o princípio da justiça, então não pode haver — bem como jamais poderá haver — padrão moral algum por meio do qual qualquer controvérsia entre dois ou mais seres humanos possa ser solucionada de modo que o resultado seja obrigatório às partes envolvidas; e, em virtude disso, o inevitável destino da raça humana deve ser sempre permanecer em guerra; sempre esforçar-se para saquear, escravizar e assassinar uns aos outros; com nenhum instrumento além da fraude e da força para dar um término ao conflito.

### Seção X

Se não existir a obrigatoriedade da justiça, então certamente não pode haver nenhuma obrigação moral — a verdade, a compaixão ou qualquer outra — que recaia sobre a humanidade. Negar a obrigatoriedade da justiça, portanto, significa negar a existência de qualquer obrigação moral entre os homens em suas relações uns com os outros.

### Seção XI

Se não existir o princípio da justiça, então o mundo é um mero abismo de escuridão moral; sem sol, luz e regras para guiar os homens em sua conduta perante os seus semelhantes. Em suma, se não existir, na natureza, o princípio da justiça, então o homem não possui natureza moral; e, em razão disso, não possui quaisquer deveres morais.

## Capítulo III — O Direito Natural Contrastado com a Legislação

### Seção I

O direito natural — a justiça natural —, sendo um princípio que é naturalmente aplicável e adequado para a resolução legítima de todas as controvérsias que possam surgir entre os homens; sendo também o único padrão pelo qual qualquer controvérsia entre os homens pode ser legitimamente resolvida; sendo um princípio cuja proteção todo homem exige para si mesmo, estando disposto ou não a acordá-lo com os outros; sendo também um princípio imutável, um que é sempre e em todo lugar o mesmo, em todas as eras e em todas as nações; sendo, de forma autoevidente, necessário em todas as eras e em todos os lugares; sendo tão inteiramente imparcial e equitativo em relação a todos; sendo tão indispensável à paz da humanidade em todo lugar; sendo tão vital à segurança e ao bem-estar de todo ser humano; sendo, também, tão facilmente aprendido, tão universalmente conhecido e tão tranquilamente mantido por associações voluntárias que todos os homens podem pronta e legitimamente formar para esse propósito; sendo ele um princípio como esse, surgem então estas questões: Por que ele não prevalece universalmente ou quase universalmente? Por que ele não foi, eras atrás, estabelecido ao redor do mundo como a única lei a que qualquer homem — ou todos os homens — poderia ser legitimamente compelido a obedecer? Por que algum ser humano concebeu que algo tão autoevidentemente supérfluo, falso, absurdo e atroz — como todas as legislações devem necessariamente ser — pudesse ter alguma utilidade à humanidade ou ter qualquer lugar nas relações humanas?

### Seção II

A resposta é que, através de todas as eras históricas, onde quer que quaisquer pessoas tenham avançado além do estágio selvagem e aprendido a aumentar os seus meios de subsistência pelo cultivo do solo, um número maior ou menor delas se associou e se organizou como saqueadoras, para roubar e escravizar todas as outras que haviam acumulado alguma propriedade que pudesse ser tomada ou que haviam demonstrado, através do trabalho, que poderiam contribuir para o sustento ou o prazer daquelas que as escravizariam.

Esses bandos de saqueadores, em baixo número no começo, aumentaram o seu poder unindo-se uns aos outros, inventando armas de guerra, disciplinando-se nas artes bélicas, aperfeiçoando as suas organizações como forças militares e dividindo os seus saques (incluindo os seus cativos) entre si mesmos, em proporção anteriormente acordada ou na forma como os seus líderes (sempre desejosos de aumentar o número dos seus seguidores) ordenassem.

O sucesso desses bandos de saqueadores foi fácil pelo fato de que aqueles que eram saqueados e escravizados não tinham proteção; encontravam-se fragmentados ao longo do território; estavam empenhados totalmente, por implementos rudes e trabalho pesado, em retirar do solo o seu sustento; não tinham armas de guerra além de paus e pedras; não possuíam disciplina militar ou organização bélica; e não tinham nenhum meio de, quando repentinamente atacados, concentrarem as suas forças ou agirem em concerto. Sob essas circunstâncias, a única alternativa que lhes restava para salvar as suas vidas ou as vidas das suas famílias era não somente entregar as suas colheitas e as terras que cultivaram, mas também entregar a si mesmos e as suas famílias como escravos.

Daí em diante, os seus destinos foram, como escravos, cultivar para os outros as terras que anteriormente cultivaram para si mesmos. Levados constantemente ao trabalho, a riqueza lentamente cresceu, mas toda ela foi parar nas mãos dos tiranos.

Esses tiranos, vivendo somente por meio do saque e do trabalho dos seus escravos e aplicando todas as suas energias na obtenção de ainda mais saques e na escravização de outras pessoas indefesas; aumentando também em quantidade — bem como aperfeiçoando e multiplicando as suas organizações e as suas armas de guerra —, eles estenderam e ampliaram as suas conquistas até o momento em que se tornou necessário, para assegurar o que eles já haviam conquistado, que agissem sistematicamente e cooperassem uns com os outros para o propósito de manterem os seus escravos em sujeição.

Tudo isso, porém, eles apenas poderiam fazer estabelecendo o que chamaram de governo e elaborando o que chamaram de leis.

Todos os grandes governos do mundo — tanto aqueles que ora existem como os que desapareceram — tiveram essa característica. Eles eram meros bandos de saqueadores que se associaram com o propósito de roubar, conquistar e escravizar os outros homens. E as suas leis, como eles as chamaram, eram apenas acordos nos quais eles consideraram conveniente entrar para manterem as suas organizações e agirem em conjunto no saque e na escravização dos outros, bem como para assegurarem a cada um a sua parte acordada dos espólios. ^[Lembremos a história de Santo Agostinho acerca do diálogo entre Alexandre, o Grande, e um pirata: Removida a justiça, o que são os reinos senão grandes assaltos, e os roubos, senão pequenos reinos? O bando, em si, é formado por homens; é governado pela autoridade de um príncipe; é unido pelo pacto de uma confederação; o butim é dividido pela lei pactuada. Se, pela admissão de homens abandonados, esse mal aumenta a tal grau que domina lugares, fixa estabelecimentos, toma posse de cidades e subjuga povos, ele assume mais claramente o nome de reino, porque essa é a realidade que lhe é manifestamente conferida — não pela remoção da cobiça, mas sim pela adição da impunidade. Com efeito, foi uma resposta adequada — bem como essencialmente verdadeira — aquela que um pirata capturado deu a Alexandre, o Grande. Quando esse rei lhe perguntou o que queria dizer com “manter a posse hostil do mar”, o pirata respondeu com orgulhosa insolência: “O mesmo que tu queres dizer quando falas da conquista da Terra inteira; mas, porque eu o faço com um pequeno e insignificante navio, eu sou chamado de ladrão, ao passo que tu, que o fazes com uma grande frota, és chamado de imperador.” (N. do R.)]

Todas essas leis não tinham mais obrigatoriedade do que têm os acordos nos quais os salteadores, bandidos e piratas consideram necessário entrar com os outros para serem mais bem sucedidos em seus crimes e dividirem mais pacificamente os seus espólios.

Assim, substancialmente, toda legislação do mundo teve a sua origem nos desejos de uma classe — a das pessoas que saqueavam e escravizavam as outras e as mantinham como propriedade.

### Seção III

No decorrer do tempo, a classe saqueadora ou detentora de escravos — que roubou todas as terras e controlava todos os meios de criação de riqueza — começou a descobrir que o modo mais fácil de controlar os seus escravos e de torná-los lucrativos não era que cada um dos donos de escravos mantivesse o seu número especificado de escravos, como eles haviam feito até então e como eles manteriam tantos rebanhos, mas sim que se lhes fosse dada tanta liberdade quanto necessária para ser jogada sobre eles a responsabilidade pela sua própria subsistência e que eles, ainda, fossem compelidos a vender o seu trabalho à classe detentora de terras — os seus antigos donos — pelo que esses últimos escolhessem lhes dar.

É claro, esses escravos libertos, como alguns erroneamente os chamaram, não tendo terras ou outras propriedades e nenhum meio de obter uma subsistência independente, não tinham nenhuma alternativa — para se salvarem da fome — além de venderem o seu trabalho aos donos de terras em troca apenas das mais rudes necessidades da vida; e nem sempre tanto quanto isso.

Esses escravos libertos, como eram chamados, agora não eram menos escravos do que antes. Os seus meios de subsistência eram, talvez, até mais precários do que quando cada um tinha o seu próprio dono, o qual nutria o interesse de preservar a sua vida. Eles estavam sujeitos, pelo capricho ou interesse dos donos de terras, a serem expulsos da casa e do emprego e a serem privados da oportunidade de até mesmo obter a própria subsistência pelo trabalho. Eles eram, portanto, em grande número, levados a esmolar, roubar ou morrer de fome; e se tornaram, é claro, perigosos à propriedade e à tranquilidade dos novos mestres.

A consequência foi que esses novos mestres acharam necessário, para a sua própria segurança e para a proteção das suas propriedades, organizarem-se mais perfeitamente como um governo e assim fazerem leis para manter essas pessoas perigosas em sujeição; isto é, leis fixando os preços pelo qual eles seriam compelidos a trabalhar e prescrevendo também temíveis punições, até mesmo a morte, por roubos e invasões que eram levados a cometer, como únicos meios de salvarem-se da fome.

Essas leis continuaram em vigor por centenas de anos e, em alguns países, por milhares de anos; e elas se encontram em vigor hoje em dia, em maior ou menor grau, em praticamente todos os países do globo.

O propósito e o efeito dessas leis eram o de assegurar, nas mãos da classe saqueadora ou dona de escravos, um monopólio de todas as terras e, tanto quanto possível, de todos os outros meios de criação de riqueza; e, assim, o de manter a grande massa de trabalhadores em tal estado de pobreza e dependência que essa situação os compeliria a vender o seu trabalho para os tiranos pelos preços mais baixos com os quais a vida podia se sustentar.

O resultado de tudo isso é que a pouca riqueza que há no mundo encontra-se nas mãos de uns poucos — isto é, nas mãos da classe legisladora, dona de escravos; eles são agora tão donos de escravos em espírito quanto sempre foram, mas alcançam os seus propósitos por meio das leis que fazem para manter os trabalhadores em submissão e dependência, em vez de cada um manter os seus escravos individuais, bem como tantas propriedades.

Assim, o negócio da legislação, que agora tem gigantes proporções, teve a sua origem nas conspirações, as quais sempre existiram entre os poucos, para o propósito de manter os muitos em sujeição e para extorquir deles o seu trabalho e todos os seus lucros.

Os reais motivos e o verdadeiro espírito que estão na base de toda legislação — apesar de todas as pretensões e de todos os disfarces atrás dos quais eles tentam se esconder — são os mesmos hoje como foram sempre. O maior propósito da legislação é simplesmente manter uma classe de homens em subordinação e servidão à outra.

### Seção IV

O que, então, é a legislação? É a presunção de um homem ou um grupo de homens de domínio absoluto e irresponsável sobre todos os outros homens que eles dizem estarem submetidos ao seu poder. É a presunção de um homem ou um grupo de homens do direito de sujeitar todos os outros homens às suas vontades e de fazê-los ficarem ao seu serviço. É a presunção de um homem ou um grupo de homens de um direito de abolir totalmente todos os direitos naturais, toda a liberdade natural dos outros homens; de fazer com que todos os outros homens sejam os seus escravos; de ditar arbitrariamente a todos os outros homens o que eles podem ou não fazer; o que eles podem e não podem ter; o que eles podem e não podem ser. É, em suma, a presunção de um direito de banir o princípio dos direitos humanos, o princípio da própria justiça, da face da Terra e de estabelecer, no lugar dele, os seus prazeres, as suas vontades e os seus interesses pessoais. Tudo isso — e nada menos — está envolvido na própria ideia de que possa existir algo como uma legislação humana e de que essa seja obrigatória para todos aqueles a quem é imposta.

# Sobre o autor
**Lysander Spooner** (19 de janeiro de 1808, Athol, Massachussets — 14 de maio de 1887, Boston, Massachussets) foi um advogado, jurista, empreendedor, abolicionista e anarquista individualista americano. Ele defendeu a inconstitucionalidade da escravidão nos Estados Unidos e desafiou o monopólio do serviço postal do governo, além de ter sido um dos maiores teóricos do jusnaturalismo.

Lysander Spooner possui grandes honrarias na história do pensamento político. Por um lado, ele, indiscutivelmente, foi o único constitucionalista na história que evoluiu ao estágio de anarquista individualista; por outro lado, ele tornou-se, progressiva e inexoravelmente, cada vez mais radical à medida que envelhecia.

Spooner publicou Direito Natural ou A Ciência da Justiça como um folheto no ano de 1882. Esse folheto influenciou de modo considerável os anarquistas americanos e europeus da época. O plano de Spooner era que tal folheto servisse de introdução para um amplo tratado sobre o direito natural da liberdade; e o fato de que Spooner não pôde viver o suficiente para completar o seu projeto é uma das grandes tragédias da história do pensamento político. Mas a sua obra mantém o seu duradouro valor graças ao fato de que, de todos os teóricos da tradição lockeana do direito natural, Spooner foi o único a conduzir a teoria à sua conclusão lógica e infinitamente radical: o anarquismo individualista.