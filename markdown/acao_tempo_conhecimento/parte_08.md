## Capítulo 8 - HAYEK, O EFEITO RICARDO E O EFEITO CONCERTINA

I.  Introdução

     

       No debate travado entre ambos, nos anos trinta do século passado, Keynes afirmava que a recessões são provocadas por investimentos de menos e poupança de mais, enquanto Hayek sustentava que elas são causadas por investimentos de mais e poupança de menos.  Evidentemente, posições assim tão antagônicas confundem facilmente não apenas os leigos, mas também muitos economistas e, ao mesmo tempo, mostram como a economia é um campo de estudos fascinante.

 

      Conforme comentamos em capítulos anteriores, uma das maiores dificuldades da maioria dos economistas da mainstream é entender a Teoria Austríaca do Capital e, consequentemente, a Teoria Austríaca dos Ciclos Econômicos, porque esta última integra a primeira com as teorias da moeda e do processo de mercado.

 

      Em particular, temos observado que existe uma dificuldade muito grande por parte dos economistas — até mais do que entre os leigos — em compreender o insight austríaco, segundo o qual variações no estoque de moeda e na taxa de juros afetam a economia de modo não uniforme.  A dificuldade é porque, em praticamente todos os casos, os economistas recebem uma formação que divide a economia em macro e micro e sequer estudam a teoria do capital. 

 

      Em Monetary Theory and the Trade Cycle, de 1933 e em Prices and Production, de 1937, Hayek salientou que as flutuações cíclicas podem ser provocadas por fatores monetários, isto é, por aumentos da oferta de moeda não lastreados em poupança e que o fenômeno das flutuações, embora tendo causas monetárias, manifesta-se por alterações no setor real da economia, em sua estrutura real de produção ou estrutura de capital.  Naqueles livros, Hayek mostrou serem diferentes os impactos de uma queda na taxa de juros causados por aumentos na oferta de moeda e no aumento da poupança: os primeiros provocam flutuações cíclicas, os segundos não; os primeiros são maléficos, enquanto os segundos são benéficos.  Esses impactos de uma expansão monetária não lastreada em poupança sobre a estrutura de produção da economia podem ser chamados de efeito taxa de juros. 

 

      Mais tarde, em Profits, Interest and Investment, em 1939 e em The Pure Theory of Capital, em 1941, reconhecendo a existência de lacunas nos dois livros anteriores, Hayek direcionou a atenção para os impactos que as variações nos preços relativos provocados pelas expansões monetárias causam sobre as decisões de investimentos.  Esses impactos constituem o efeito preços relativos.

   

II.  O efeito Ricardo

      Os dois efeitos são plenamente compatíveis um com o outro e ambos acontecem no contexto do chamado efeito Ricardo, para o qual há duas interpretações: a original, formulada por David Ricardo no início do século XIX e a proposta por Hayek.

 

      Segundo a teoria do valor-trabalho, que Ricardo importou de Adam Smith, os preços relativos são determinados pela quantidade de trabalho requerida para produzir cada produto.  Para Ricardo, no entanto, isso poderia não acontecer quando se utiliza capital, pelo motivo de que uma máquina capaz de produzir, por um método indireto, a mesma quantidade de produto que, por exemplo, cem trabalhadores/mês podem produzir por um método direto, requer um número de trabalhadores inferior a cem/mês, pois, em caso contrário, não haveria razão para se utilizar a máquina. 

 

      Consequentemente, um aumento nos salários aumenta o custo dos cem trabalhadores em um montante menor do que o crescimento no custo da máquina.  O efeito Ricardo refere-se ao fato desse aumento de salários, ao mesmo tempo, encorajar a substituição de homens por máquinas e diminuir os preços dos bens produzidos com o uso da máquina relativamente aos preços dos bens produzidos por processos mais diretos, sem a sua utilização. 

 

      Esse efeito também funciona no sentido oposto: elevações nos preços dos bens de consumo final diminuem os salários reais, fazendo com que as máquinas sejam substituídas por trabalhadores.  O efeito Ricardo, portanto, em sua formulação original, refere-se à substituição de homens por máquinas quando os salários aumentam e/ou os preços dos bens de consumo final caem.

 

      Entretanto, para Hayek, que trabalhou dentro da perspectiva da Teoria do Capital Austríaca, o efeito não pode se restringir meramente à substituição entre mão de obra e máquinas, mas à substituição entre métodos de produção mais indiretos (roundabout) e métodos menos indiretos.  Não é correta, portanto, a impressão de que a teoria hayekiana dos ciclos econômicos é uma simples discussão acerca das variações na proporção entre capital e trabalho ao longo do tempo.    

 

      A relevância da expansão monetária torna-se evidente: a implantação bem sucedida de métodos de produção indiretos requer uma provisão prévia de recursos sob a forma de poupança voluntária.   A poupança forçada, definida como expansões no crédito não lastreadas em expansões na poupança, termina gerando uma inflação nos preços dos bens de ordens mais baixas e reduz os salários reais e, portanto, não é uma boa alternativa, porque o efeito Ricardo atua cumulativamente contra os métodos de produção mais indiretos.  Por isso, um boom de investimentos desencadeado por expansão monetária está fadado ao fracasso.  As políticas keynesianas ditas de pleno emprego são implausíveis, exatamente por causa do efeito Ricardo.

 

      Em resumo, o efeito Ricardo original refere-se à substituição de homens por máquinas, quando os salários nominais aumentam e/ou os preços dos bens caem, enquanto o efeito Ricardo hayekiano diz respeito à substituição de métodos mais indiretos por métodos menos indiretos, em decorrência de aumentos salariais e/ou quedas nos preços. 

 

 As proposições de Hayek

 

      Para efeitos didáticos, podemos resumir as principais proposições de Hayek:

 

       (1ª) As recessões são causadas pelo encurtamento dos processos de produção (efeito concertina), cuja causa principal é o fenômeno da poupança forçada que, por sua vez, é provocada pela nova moeda posta em circulação, cujos efeitos benéficos são temporários.

  

      (2ª) Aumentos na poupança voluntária alargam permanentemente os processos de produção indiretos.  Em contraste com Keynes, poupar faz "bem" à economia!

   

      (3ª) Um aumento na demanda de bens finais causado por um crescimento não neutro na oferta de moeda encurta inevitavelmente os processos de produção (efeito concertina) e leva futuramente, também inevitavelmente, a uma recessão na economia. 

 

      (4ª) Níveis excessivamente altos de gastos públicos e de impostos aumentam a relação gastos/poupança, encurtam os processos de produção (novamente, o efeito concertina) e levam futuramente à recessão.

 

      (5ª) A oferta de moeda não deve variar, exceto o necessário para contrabalançar as variações na "velocidade de circulação", os efeitos provocados pela integração nos negócios e as eventuais mudanças em métodos de pagamentos.

 

      (6ª) Uma expansão monetária efetuada fora esses casos é danosa, por encurtar, após algum tempo, os processos de produção (outra vez, o efeito concertina).

 

      (7ª) Crescimentos na produção e no comércio não justificam aumentos no crédito bancário.

 

      (8ª) O governo não deve tentar enfrentar as recessões fazendo reflações monetárias, isto é, emitindo moeda, porque tais medidas apenas tenderiam a agravar o problema, uma vez que o efeito concertina iria se repetindo enquanto o governo persistisse em corrigir os desequilíbrios causados pela expansão monetária sem lastro com expansões monetárias adicionais.

   

 

III.  Efeito taxa de juros

  

      À medida que a taxa de juros cai, os retornos aumentam em geral, mas os processos mais indiretos ficam relativamente mais lucrativos.  A isto os austríacos denominam de capital deepening, ou aprofundamento do capital, para indicar que a base do triângulo de Hayek se alarga, com a criação de novos estágios de produção à esquerda, mais afastados, portanto, do estágio do bem de consumo final ou bem de primeira ordem.

 

      E, conforme a taxa de juros aumenta, os retornos caem em geral, mas com um viés em favor dos processos de produção mais diretos.  Nesse caso, tende a acontecer um estreitamento da base do triângulo de Hayek, ou capital shallowing.

 

      Existe, portanto, uma assimetria nos switches de curto para longo prazo e de longo para curto prazo, ou seja, variações na taxa de juros não afetam de maneira proporcional todos os setores da estrutura de produção.  As variações na taxa de juros afetam a economia de uma forma desigual ao longo da estrutura de produção.  Isso não é considerado pelos modelos macroeconômicos, em que mudanças na taxa de juros afetam toda a economia por igual, de modo uniforme.

 

      Simbolicamente, sejam y o rendimento e t, t-1 e t -2 três estágios consecutivos da estrutura de produção.  Em equilíbrio, então, teríamos:

 

                                              y t-2  =  y t-1 =  y t

 

      Cada um desses rendimentos ou yields é um valor presente; por exemplo, sendo A a margem não descontada e  r  a taxa  de  desconto  (taxa de juros), pode-se escrever:

 

y t-2  =  A t-2 ( 1+r  )-2   ;    y t-1 = A t-1 ( 1+r )-1 ; e y t = A t (1 + r)

  

      Se as condições iniciais são: y t-2 =  y t-1 = y t, à taxa de desconto r 0, então:

 - a uma taxa de desconto r1 < r 0  teríamos y t-2 > y t-1 > y t, indicando que os projetos com maior duração serão mais beneficiados do que os de curta duração   e

 

- a uma taxa de desconto r 2 > r 0 teríamos y t-2 < y t-1< y t , sugerindo que os projetos de prazo mais longo são mais prejudicados do que os de prazo mais curto. 

 

      No gráfico seguinte, VP é o valor presente de dois projetos (um "longo" e um "curto") e y é o rendimento.  A uma dada taxa de desconto (que não aparece no gráfico), os dois valores presentes são iguais, para um dado rendimento.  Evidentemente, a elasticidade dos projetos de prazos maiores é menor do que a dos projetos de prazos menores.

  

26.jpg

 IV.  Efeito preços relativos

 

      O aprofundamento dos processos de produção permite obter quantidades de produto maiores a partir de um dado volume de fatores de produção; mas estes bens só estarão disponíveis posteriormente, e tanto mais posteriormente quanto mais indireto for o processo de produção.  Eis a decisão econômica: é mais lucrativo manter ou alterar a estrutura de produção?  A resposta vai depender da comparação entre o preço recebido pelo bem final e os preços que devem ser pagos pelos bens intermediários.

 

      Tomemos o caso de uma expansão na oferta de moeda.  Ela reduz a taxa de juros, o que aumenta o grau de roundaboutness, isto é, acontece um alargamento da estrutura de produção.  Com isso, os preços dos bens finais irão subir comparativamente aos preços dos bens mais distantes do consumo final, o que elevará os rendimentos nos setores produtores dos primeiros e provocará, assim, uma redução no grau de roundaboutness.   Hayek denominou isto, como vimos,  de efeito Ricardo que, em sua formulação original,  referia-se à substituição de mão-de-obra (fator de produção de curto prazo) por capital (fator de produção de longo prazo), em decorrência de uma redução na taxa de juros.  Mas, para Hayek e os austríacos, a substituição relevante não é entre "homem" e "máquina", mas entre bens de capital de ordens menos elevadas e de ordens mais elevadas ao longo da estrutura de capital.  Na fase inicial do ciclo, a taxa de juros artificialmente baixa estimula os investimentos em bens de capital em estágios mais afastados do consumo final.  Isto provocará uma disputa por bens de capital de ordens mais baixas — complementares aos de ordens mais elevadas —, fazendo subir os seus preços, o que provoca um aumento na demanda por crédito (desperation borrowing) e o subsequente aumento da taxa de juros, o que, por sua vez, encoraja a liquidação dos projetos de produção iniciados na primeira fase, mas ainda não terminados.

 

      O economista G. R. Steele (The Economics of Friedric Hayek, MacMillan Press, Londres, 1996, cap. 8) nos dá um exemplo simples desse fenômeno. 

 

      Sendo x0 o custo do investimento em t=0, B o valor da receita líquida contínua da venda de bens finais, n o ponto do tempo no futuro em que a receita deixará de existir e r a taxa de juros, então:

                                                          

27.jpg
 

      Steele admite que os níveis de investimentos estejam em seus ótimos, com cada unidade marginal (R$100) proporcionando uma taxa interna de retorno (TIR) igual à taxa de juros de mercado (em seu exemplo, 7%).  Podem se encontrar valores para B para qualquer método de produção e, selecionando valores para n e utilizando a equação acima, pode-se escrever:

 

28.jpg
 

      A estrutura de capital está em equilíbrio.  Para verificar o impacto de um aumento nos preços dos bens finais, podemos fazer cada B subir, por exemplo, 5% e, levar os novos valores para a mesma equação.  As novas taxas internas de retorno serão:



 29.jpg

      Podemos verificar que todos os rendimentos são agora maiores do que o original (que foi admitido ser de 7%).  Há, assim, incentivos para investir em todos os métodos de produção (capital widening ou capital deepening), mas o incentivo é maior para os métodos menos indiretos (capital shallowing).

 

      O efeito Ricardo produz, então, um impacto inicial de aumentar o produto (embora o nível de investimento não mude), mas produz o fenômeno do capital shallowing ou efeito concertina — em português, algo como efeito sanfona, já que a concertina é um instrumento musical com fole, semelhante a um acordeão, em que, ao abrir-se o fole pressionando um botão, obtém-se uma nota musical e, ao fechar o fole, tem-se outra nota. 

 

      O efeito concertina refere-se, portanto, ao fato de que a poupança forçada incentiva inicialmente métodos de produção mais indiretos, mas, após algum tempo, os investimentos acabam sendo realocados para os métodos menos indiretos, fazendo com que a estrutura de capital "estique" e "encolha", tal como uma sanfona.  No final das contas, o estoque de capital "agregado" ou capital fixo diminui.   Esta proposição é que os economistas keynesianos, entre eles Kaldor, que criticou a teoria hayekiana, não foram capazes de entender. 

 

      A conclusão é que a expansão monetária e a queda da taxa de juros encorajam investimentos em capital em geral, especialmente os mais indiretos, mas o efeito subseqüente de elevação dos preços dos bens finais tende a anular este viés, antes mesmo que a taxa de juros aumente.  No fim, vem a recessão, mas a facilidade de recursos e a queda dos rendimentos nos estágios de bens finais deflagram o efeito Ricardo reverso.  E começam novamente a se tornar atrativos os investimentos em métodos de produção mais indiretos.

 

      O boom artificial induzido pela expansão monetária provoca, após algum tempo, então, distorções consideráveis na estrutura de produção.  Mesmo antes do aumento na taxa de juros, as subidas nas taxas de retorno fazem com que projetos investimentos de investimentos que pareciam lucrativos tornarem-se não lucrativos e serem abandonados.  Quando a taxa de juros subir — o que acontecerá em decorrência da disputa pelo crédito entre os setores mais próximos e os mais afastados dos bens de consumo final — ocorrerá uma aceleração nesse processo.  Adicionalmente, os efeitos da queda na renda nesses setores agora não mais lucrativos causarão queda na demanda de bens de consumo final e mais desemprego.  A queda na demanda de bens intermediários da estrutura de produção, gerada pela demanda de bens finais menor, será mais um agravante. 



 

Representação gráfica do efeito taxa de juros

 

       Suponhamos que a expansão monetária reduza a taxa de juros de r para r'.  Os recursos migrarão dos projetos curtos para os longos, o que, admitindo que a eficiência marginal do capital seja decrescente, fará crescer os rendimentos dos projetos de curtos e cair as dos longos, deslocando as curvas e eliminando os diferenciais entre as taxas de retorno.  Com isso, o equilíbrio se desloca do ponto A e vai para o ponto B, onde os valores presentes dos dois projetos são novamente iguais.

 

      Mas o ponto B não configura um equilíbrio estável, porque tanto a migração de recursos para novos investimentos como o switch no investimento fazem o produto final cair.  Com a demanda de bens finais constante, ou, mesmo, aumentando caso o boom de crédito reduza o desemprego, os preços dos bens finais subirão.

  

30.jpg

 

 

Representação gráfica do efeito preços relativos

 

      Quando os preços aumentam, os valores proporcionais dos rendimentos futuros crescem e, com isso, os valores presentes dos dois projetos.  Mas os rendimentos aumentam diferenciadamente.  Uma elevação de x % no preço aumenta o retorno de y' para y''' no projeto curto e de y' para y'' no longo.  Se não houvesse limites para os fundos de investimentos, o equilíbrio, à nova taxa de desconto de r', iria do ponto A para o ponto C.

 

      No entanto, como os recursos são limitados, o incentivo provocará um switch dos projetos de longos para os curtos que, dado que a eficiência marginal do capital é decrescente, fará o yield do projeto longo subir e o do projeto curto cair, o que deslocará as duas curvas.

 

      Comparativamente às curvas que se cortam em C, a curva do projeto curto se desloca para baixo e a curva do projeto longo se desloca para cima.  A nova combinação ótima de investimentos ocorrerá em um ponto como D, que admite, por sua localização, que não haja variações no fluxo de investimento.

 

31.jpg

 

V.  Conclusões

 

      Este capítulo é uma extensão um pouco mais sofisticada de alguns aspectos do capítulo anterior, em que expusemos a Teoria Austríaca dos Ciclos Econômicos, que diagnostica as causas das flutuações cíclicas da economia nas expansões de moeda e crédito não lastreadas em um correspondente aumento na disposição de poupar dos indivíduos e empresas. 

 

      Ao ingressar na economia, a moeda "nova" provoca o efeito de diminuir a taxa de juros e, assim, estimular os projetos de longa maturação mais do que proporcionalmente aos de custo prazo.  A base da estrutura de produção se "alarga".  Contudo, quando a artificialidade da taxa de juros é descoberta pelos agentes no processo de mercado, isto é, quando eles percebem que não se tratava de "mais poupança", mas apenas de "mais moeda fantasiada de poupança", surge a quebra de coordenação, que faz com que a taxa de juros suba, o que desestimula os investimentos realizados anteriormente nos estágios mais afastados da estrutura de produção, que deixam de se tornar lucrativos.  A estrutura de produção "encolhe".  É o efeito concertina em ação.

 

      O efeito Ricardo hayekiano não diz respeito à substituição de trabalhadores por máquinas no decorrer dos ciclos, mas à substituição de métodos mais indiretos (roundabout) por métodos menos indiretos, em decorrência dos ajustamentos impostos ao setor real da economia pela ausência de coordenação provocada pela poupança forçada.  
      