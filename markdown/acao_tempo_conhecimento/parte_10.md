## Capítulo - 10 ORDEM SOCIAL, LIBERDADE E VIRTUDE

I.  Introdução

 

                   Uma sociedade livre e virtuosa — ou, para seguirmos a nomenclatura de Hayek —, uma sociedade de indivíduos livres e virtuosos — deve ter uma ordem social sustentada em princípios, valores e instituições que lhe garantam a própria essência de liberdade coresponsável, liberdade de (e não liberdade para) e que estimulem a prática das virtudes, com os consequentes subprodutos de progresso, respeito aos direitos individuais, cooperação e respeito inalienável à dignidade da pessoa humana.  O que entendemos por uma ordem social livre e ao mesmo tempo virtuosa?  Como definir liberdade e virtude nos contextos dos três grandes subsistemas que compõem as sociedades, a saber, o econômico, o político e o cultural-ético-moral?  Os postulados da Escola Austríaca atendem a essas exigências?

 

            Este capítulo procura esclarecer esta questão, tentando lançar alguma luz nas discussões que, embora algumas vezes bem intencionadas, costumam ser ponteadas por confusões conceituais e pela prevalência linguagem "politicamente correta", nem sempre rica em boas intenções. 

 

            Como o leitor poderá perceber, existe um alto grau de compatibilidade entre os princípios, valores e instituições expostos, que caracterizam uma ordem social condizente com a liberdade individual e as virtudes e os valores, princípios e instituições que fazem parte da tradição da Escola Austríaca, que pode ser identificada com a chamada economia personalista, por sua ênfase no individualismo metodológico.

 

            A economia personalista é uma abordagem do ordenamento social inspirada por séculos de tradição de pensamento antropológico judaico-cristão sobre os aspectos éticos da vida social, política e econômica.  Preocupa-se com o desenvolvimento de um entendimento mais profundo da dimensão moral das atividades econômica, política e cultural.  Por essa razão, seus estudiosos buscam o desenvolvimento de um diálogo interdisciplinar entre a doutrina social cristã, a tradição do direito natural e as melhores visões da economia de mercado (com destaque para a Escola Austríaca), do estado de direito e da democracia representativa. 

 

            Nessas bases, a economia personalista pode ser descrita como um método de reflexão, sem separar fé e razão, sobre os dilemas morais, econômicos e políticos propostos pela modernidade.  Como postura filosófica, no entanto, a economia personalista repousa sobre a tradição humanista ocidental e, portanto, é definida pelo desejo de ajudar a realizar uma economia livre e humana, no contexto de uma sociedade livre e virtuosa.  Pressupõe a economia personalista que todas essas instituições devem se fundamentar em uma antropologia que reflita a dignidade da pessoa humana.  Um de seus objetivos, portanto, é demonstrar que a tradição ocidental — que repousa na herança das civilizações da antiguidade clássica e da cristandade medieval — ainda guarda uma imensa atualidade, desde que se abstenha das premissas utilitaristas, coletivistas, relativistas e do racionalismo construtivista e utópico sobre as quais, infelizmente, têm se baseado.

 

            Sob esse aspecto, a economia personalista busca complementar a visão de economia livre com a de uma antropologia cristã de nítidos contornos na fé e na reta razão.  Ela reconhece, utilizando o individualismo metodológico, que a tradição do direito natural é uma das formas de comunicar essa visão nas sociedades pluralistas, caracterizadas por diferenças significativas nas crenças fundamentais, mas que trazem a unidade da moral judaico-cristã como base dos valores culturais.  Como vemos, a economia personalista trabalha com muitos dos conceitos básicos da Escola Austríaca.

 

            Ao contrário do que normalmente se pensa, a Escola Austríaca de Economia tem uma sólida fundamentação moral.  Seus elementos mais importantes, como os conceitos básicos de ação humana e de limitação do conhecimento, seu enfoque subjetivista, sua preocupação em valorizar as ordens espontâneas, seu esforço para conter o excesso de poder do estado sobre os indivíduos e sua defesa das liberdades individuais encaixam-se perfeitamente nos ensinamentos morais mais importantes que serviram de sustentação para a civilização ocidental, especialmente os da tradição judaico-cristã e, em especial, os da denominada Doutrina Social da Igreja, interpretada de maneira reta, o que vale dizer, escoimada das deturpações da denominada "teologia da libertação".

 

                  Acreditamos que a principal premissa a ser posta, que representa o pilar de muito do que se dirá em seguida, é a da prevalência dos valores éticos e morais da civilização ocidental herdados da tradição judaico-cristã sobre a qual a sociedade ocidental sempre repousou.  Com efeito, dos três sistemas mencionados, embora cada um tenha seus próprios mecanismos e leis de funcionamento, sabemos que o sistema social é uma complexa interação entre eles e, sem dúvida, o sistema ético-moral-cultural — em particular, a rica tradição do ocidente, da qual a chamada Doutrina Social da Igreja representa uma síntese bastante satisfatória —, deve servir de pano de fundo para o sistema político e o sistema econômico.

 

                  Os economistas austríacos sempre argumentaram, corretamente, que a ciência econômica deve ser neutra em relação aos aspectos morais.  Sua preocupação deve ser analisar as escolhas inerentes à ação humana dos indivíduos nos mercados, ao longo do tempo e sob condições de incerteza genuína.  Assim, ela deve ter a capacidade de explicar qualquer mercado, seja qual for o bem ou serviço transacionado nele.

 

                  Mas um ato econômico pode ser, sob o ponto de vista moral, "bom", "mau" ou "neutro".  Por exemplo, a compra ou a venda de fraldas para bebês é um ato moralmente bom, porque a fralda irá servir a uma criança inocente; já a compra ou a venda de cocaína, que também é uma ação no sentido que a Escola Austríaca dá a esse conceito, é moralmente má; e a compra ou venda de um tubo de creme para barbear, em princípio, pode ser considerada como moralmente neutra.  Evidentemente, as ações políticas também podem ser moralmente más, boas ou neutras, assim como todas as ações praticadas por indivíduos em todos os campos.    

 

                  Como salientou o economista austríaco Israel Kirzner em conferência proferida no Rio de Janeiro, em 1993, o mercado funciona sempre, mas possui uma natureza que pode ser ética ou antiética.  De fato, mercados de mamadeiras funcionam tão bem, no sentido puramente econômico, quanto os mercados de drogas.  Nesse ponto, a economia é neutra: deve ser capaz de explicar satisfatoriamente ambos os mercados.  Mas, quando entramos em considerações sobre a condição humana, percebemos que, quando se tem em vista o bem estar e a dignidade da pessoa humana, a economia — não a economics, mas a economy — não pode ser analisada, por seu conteúdo humano, sem levar em conta suas interações com a ética, a moral e a política.  Ou seja, os economistas não podem abster-se de saber que determinados mercados são bons, por contribuírem para a dignidade dos indivíduos e que outros mercados são maus, porque agridem a dignidade da pessoa humana e prejudicam terceiros.

                    

II.  Fundamentos éticos

 

                  Liberdade e virtude, quando apresentados isoladamente, são atributos universalmente aceitos.  De fato, não nos lembramos de ter visto ou ouvido ninguém — nem mesmo os piores ditadores ou os criminosos mais contumazes — denegrir publicamente a liberdade ou enaltecer o vício.  Mas, quando se considera uma das formas mais elementares de liberdade, que é a econômica, é muito comum associá-la com licenciosidade, com permissividade, com egoísmo individualista e com muitos outros vícios.  Isso poderia levar-nos a crer que liberdade e virtude seriam dois objetivos incompatíveis.  Mas, felizmente, é exatamente o contrário que acontece.

 

                  A visão, especialmente peculiar aos denominados intelectuais, de associar liberdade econômica com vício e liberdade política com virtude, é totalmente imprópria e dá margem a interessantes reflexões, podendo ser identificada, especialmente nos países da América Latina, como uma das causas da pouca penetração das doutrinas liberais e conservadoras, como a da Escola Austríaca, que sustentam a tese da inseparabilidade e indivisibilidade da liberdade e da virtude. 

  

A moral como arte

  

                  Segundo o antropólogo espanhol Juan Luis Lorda (Moral: a Arte de Viver, Quadrante, São Paulo, 2001), há três definições equivalentes de moral.  A primeira a conceitua como a arte de viver bem, de viver em harmonia com a dignidade inerente à pessoa humana.  Mas, como a característica principal dos seres humanos é a liberdade, podemos também definir moral como a arte de educar a liberdade, ou de usá-la bem.  Por fim, como a educação da liberdade consiste em conhecer, praticar, adquirir e aprender bons costumes, podemos definir moral como o conhecimento, a prática, a aquisição e o aprendizado de bons costumes, que são, por definição, aqueles que proporcionam ao homem viver como ser humano, diferentemente dos outros animais que, por não possuírem a faculdade da razão, praticam todos os seus atos por instinto.

 

                  Observemos que nas três definições considera-se a moral como uma arte.  De fato, a moral é uma arte e o que acontece com ela não é diferente do que ocorre com as artes: sem base teórica, torna-se impossível orientar bem a prática e sem uma boa prática torna-se impossível fazer as coisas satisfatoriamente.  Para tocar-se bem o piano, é preciso primeiro adquirir conhecimentos teóricos básicos, tais como a leitura do pentagrama, a postura correta do corpo, dos pulsos e as melhores maneiras de dedilhar o teclado.  Depois, é preciso prática, muita prática, se o objetivo é o de ser um verdadeiro artista.  O que dissemos para o piano vale para qualquer arte.  E vale também para a moral.  Para viver-se bem, para educar a liberdade e para adquirir e praticar bons costumes, é necessário primeiro adquirir conhecimentos acumulados por outros e forjar hábitos que só podem ser adquiridos mediante o exercício pessoal.  A arte da moral requer conhecimentos e habilidades, teoria e prática, princípios e hábitos, vocação e disposição.

 

                  Praticamente quase toda a nossa vida moral consiste no desenvolvimento livre das capacidades que recebemos desde que fomos criados e postos neste mundo, as quais possuem as suas próprias leis, que não podemos reinventar nem alterar.  Não podemos, por exemplo, inventar como é a liberdade, a amizade e o amor, o máximo que podemos fazer, às vezes, é escolhê-los livremente.  Assim, a moral não depende de nossos gostos e preferências, nem tampouco é uma questão de simples opinião.  O aprendizado moral é bastante difícil e delicado, o que exige muito esforço para obtê-lo, mas sempre vale a pena tentar fazê-lo, pois seria realmente lamentável deixar transcorrer toda a vida sem ter tido a percepção do que ela tem de mais importante.  Há, como diz Lorda, um verdadeiro supermercado de sistemas morais.  Neste capítulo, nossa preocupação é com a moral revelada, que pressupõe a liberdade interna, e com as relações dessa moral com a Escola Austríaca, que pressupõe a liberdade externa.

 

 Liberdade exterior, liberdade interior, ignorância e fraqueza

  

                  Na pessoa humana, há dois tipos de liberdade.  O primeiro é uma liberdade que se vê ou exterior, em que dizemos ser alguém livre quando pode fazer o que sua vontade desejar, sem empecilhos, quando pode ir e vir, quando pode opinar e discordar, torcer por um determinado clube de futebol, etc.  A imensa maioria das pessoas define isto simplesmente como liberdade, por serem estas as partes que se vêem.  Mas a outra liberdade, a que não se vê ou interior, é mais importante, sob o ponto de vista moral.

 

                  A liberdade interior é a liberdade da nossa consciência, pois os impedimentos que enfrenta não estão fora, mas dentro.  Uma pessoa é livre interiormente quando pode guiar-se pela luz da sua consciência, sem obstáculos interiores que a impeçam de agir dessa forma.  Os obstáculos interiores da liberdade são a ignorância e a fraqueza, pois aquele que não sabe o que tem que fazer só tem a liberdade de errar, nunca a de acertar e aquele que é fraco termina deixando que a desarrumação dos seus sentimentos ou o medo do que "vão dizer" lhe roubem a liberdade, embora muitas vezes não seja nem capaz de perceber isso.

 

                  Tanto a ignorância quanto a fraqueza apagam a voz da consciência, lançando-a na escuridão.  De fato, não pode decidir bem quem não tem conhecimento ou base para decidir, o que explica porque as consciências deformadas ou com pouca formação moral sejam incapazes de acertar, bem como de dar demonstrações de liberdade.  Por sua vez, quem mostra uma fraqueza, tal como uma paixão desordenada pelos jogos de azar, não consegue decidir bem sobre o que deve fazer cada dia, porque essa paixão tomará a maior parte do seu tempo, assim como quem é preguiçoso não consegue enfrentar as suas obrigações, deixa-as passar, engana-se a si mesmo e esquece-as, ou quem se deixa influenciar pelo que os outros poderão vir a dizer de seus atos torna-se incapaz de qualquer ação que possa ser criticada, mesmo que essa ação seja moralmente correta.  Nenhuma dessas pessoas é realmente livre, pois nenhuma delas possui a liberdade de agir bem, apenas a de agir mal.

 

                  Portanto, para alcançar a liberdade interior, é preciso vencer a ignorância e as manifestações de fraqueza, para que a consciência funcione bem, para que descubra a verdade e seja capaz de estabelecer uma ordem entre os direitos e os deveres.

  

Liberdade situada

      

                  Os ensinamentos morais consistem em preceitos negativos, do tipo "não faças isto e aquilo" e em recomendações positivas, como "amarás a Deus sobre todas as coisas e ao próximo como a ti mesmo".  Os negativos constituem-se no princípio mínimo da moral, mas os positivos não podem ser cumpridos de uma só vez, são mais objetivos, projetos para toda a vida.  Por isso, a moral não se resume apenas a respeitar uma série de proibições, que são o seu limite mínimo, mas também não pode exigir que codifiquemos tudo o que é bom e tudo o que é mau, pois, para isso, temos a nossa consciência.  A moral apenas nos indica algo como um portão, que separa o que está fora e o que está dentro.  Cada pessoa humana está situada no mundo ou, como na célebre expressão do filósofo espanhol José Ortega y Gasset, "eu sou eu e minhas circunstâncias".  São essas circunstâncias que integram nossa moral e situam a nossa liberdade.

 

                  Mas nossa liberdade não é absoluta, pois, quando chegamos a este mundo, já o encontramos com suas leis, coisas, pessoas e tudo o mais; apenas viemos ocupar nosso lugar entre elas.  Assim, nossa liberdade é condicionada por tudo o que existia antes de nós ou, como Lorda afirma, citando Zubiri, é uma liberdade situada.  Estamos limitados por nossa natureza, por nossas habilidades ou talentos, inteligência, inclinações e debilidades, estamos condicionados por nossas origens étnicas e geográficas, por nosso ambiente de trabalho e pelas pessoas com quem convivemos.  Segue que não podemos construir a vida à margem de todos esses condicionantes: eles são o que são e como são e ponto final.

 

                  Por isso, é uma utopia pensar em uma liberdade sem restrições, pois não há ninguém que possa desfrutar dessa condição; todos têm condicionantes à sua liberdade, uns mais, outros menos e devemos encarar essas limitações como as regras do jogo da vida. 

  

As escolhas e os talentos

  

                  Portanto, cada pessoa humana possui condicionamentos e graus de liberdade próprios e, sendo assim, está em uma situação única perante a vida, o que nos impede de exigir o mesmo de cada uma.  Esta verdade, que nos remete ao individualismo metodológico da economia personalista e ao princípio da autodeterminação da pessoa humana da Doutrina Social, é magnificamente ilustrada pela parábola dos talentos (Mt., 25), em que um homem rico, antes de ausentar-se por motivo de viagem, chama seus três servos e distribui entre eles, de forma desigual, os talentos (medida de peso de ouro ou prata), ordenando que cada um os administre da melhor maneira que puderem.  Deu cinco ao primeiro, dois ao segundo e um talento ao terceiro.  Os dois primeiros empregados conseguiram dobrar os talentos recebidos, mas o terceiro limitou-se a enterrá-lo, para devolvê-lo ao patrão para quando este regressasse.  O patrão, ao retornar e após a prestação de contas, elogiou os dois primeiros empregados e repreendeu severamente o terceiro: "Servo mau e preguiçoso!"

 

                  Cada um de nós recebe uma dada quantidade de talentos, de inteligência e de habilidades e tem a vida inteira para negociar com esses dons e possui, evidentemente, a liberdade para fazê-lo de maneira moralmente correta ou incorreta.  Desses talentos todos, o maior, sem dúvida, é nossa própria vida, um tempo finito e curto perante a eternidade, em que se desenvolve o nosso ser sobre a terra, juntamente com todos os demais talentos e com a fortuna.

 

                  Assim, passamos toda a nossa existência neste mundo fazendo escolhas e é preciso fazê-las no momento oportuno.  Essas escolhas são de natureza moral, econômica e política, boas ou más: Hitler exterminou milhões de judeus, Sabin salvou milhões de crianças da paralisia infantil, Debussy compôs de maneira genial, Lênin matou ou mandou matar milhares de pessoas, Madre Teresa dedicou sua vida aos pobres, Enrico Caruso cantou esplendidamente, Pelé, Garrincha e Rivellino encantaram multidões jogando futebol, Mussolini arrastou a Itália, berço da civilização, para o precipício, Bill Evans e Oscar Peterson tocaram piano como ninguém...  Na economia, as escolhas buscam maior eficiência; no terreno moral, devem estar voltadas para a boa administração dos talentos recebidos do Criador.

  

Os diferentes conceitos de liberdade e virtude

 

                  Quando um socialista, mesmo movido pelas melhores intenções, refere-se à virtude, está, na verdade, definindo o seu conceito peculiar dessa palavra, derivado da idéia de igualdade absoluta na chegada, ou igualdade de resultados.  Para ele, virtude é sinônimo daquilo que denomina de "justiça distributiva".  Da mesma forma, quando fala em liberdade, está (mesmo que muitos socialistas não o saibam) inteiramente submerso no conceito de liberdade positiva (ou liberdade para), isto é, está aceitando a tese de que os cidadãos devem ser livres para fazer apenas aquilo que lhes for permitido fazer (permitido pelo estado, obviamente).  Assim, para um socialista — e para a maioria dos que se autodenominam socialdemocratas — o tipo de governo ideal é aquele que, mediante comandos e ordens (thesis) interfere permanentemente na economia (taxis), com o objetivo de repartir o bolo da riqueza nacional em partes preferencialmente iguais.

 

                  Já um liberal da tradição da Escola Austríaca e da economia personalista, quando se refere à liberdade, está falando de liberdade de, ou liberdade negativa, em que os indivíduos, de acordo com o princípio da dignidade da pessoa humana, devem ser livres para realizarem suas escolhas em uma economia caracterizada pela dinâmica do processo de mercado (cosmos), limitando-se o estado a garantir os direitos individuais básicos, por intermédio de normas de justa conduta (nomos), definidas com base nos usos, costumes e tradições e fundamentadas nos valores éticos e morais aceitos pela maioria das pessoas como virtudes, ao mesmo tempo em que voltadas para impedir o que é considerado um vício.

 

                  Para a tradição da economia personalista, cada indivíduo, por definição, é um universo e deve ser livre para escolher os seus próprios objetivos, desde que suas escolhas não venham a prejudicar os direitos de terceiros.  Por sua vez, em suas encíclicas denominadas de "sociais", João Paulo II faz questão de enfatizar aquilo que chama de princípio de determinação da dignidade da pessoa humana.  O homem foi criado para fazer o bem, mas apresenta uma inclinação para o mal, por isso precisa seguir padrões éticos aceitos pela sociedade, o que requer leis.  Assim, após os estágios iniciais de nossa civilização, em que se vivia isoladamente, os homens passaram a viver em sociedades, o que — dado o paradoxo definido pelas inclinações para o bem e para o mal — fez surgir a necessidade de normas gerais de justa conduta.  Como estas não poderiam ser definidas por nenhuma parte interessada, surgiu o estado, colocado acima dos interesses individuais, mas com o propósito de zelar permanentemente para que estes, quando amparados em valores morais sólidos, fossem respeitados.  Portanto, a lei — sua necessidade — é anterior à criação do estado.  A definição do que vêm a ser valores morais sólidos não deve nunca ser atributo do sistema político, isto é, do estado, nem do sistema econômico, mas sim do sistema ético-moral-cultural que deve corresponder aos valores, consagrados por séculos de usos e costumes, da tradição judaico-cristã.

 

Não há virtude sem liberdade

 

                  Imaginemos que um suposto governo instituísse algo como um "imposto solidariedade", que retiraria uma parte da riqueza dos mais ricos para distribuí-la aos mais pobres.  A própria ideia já mostra que seus mentores definem virtude (solidariedade, no caso) à sua maneira.  Onde estaria a virtude, se a boa ação (distribuir riqueza para os necessitados) não foi voluntária, mas antes compulsória?  É mais do que evidente que não haveria virtude, não haveria solidariedade, o que existiria seria uma forma de extorsão praticada pelo estado que, a partir de uma definição de virtude que não corresponde à tradição, suprimiria a liberdade de escolha dos cidadãos, obrigando-os a destinar parte da riqueza que geraram para fins definidos pelos ideólogos e burocratas.  Por outro lado, se alguém, livre e espontaneamente, distribui parte de seus bens para os mais necessitados, ou se um grupo de ajuda a pessoas carentes, ou um grupo religioso destina parte de recursos voluntariamente arrecadados para ajudar os pobres, temos aí um claro exemplo de solidariedade, que é uma virtude consagrada pela tradição judaico-cristã e não pelo estado ou pelos partidos mais radicais de esquerda.

 

                  Portanto, para que determinada ação humana possa ser classificada como solidária, ou enquadrada no conceito de virtude, ela precisa, antes de qualquer outra consideração, ser voluntária.  Isto nos remete à definição de ação de Mises: qualquer ato voluntário praticado na expectativa de se passar de um estado menos satisfatório para um estado mais satisfatório.   Assim, se alguém resolve destinar, por livre e espontânea vontade, dez por cento de sua riqueza para os pobres, pois isso o deixará mais satisfeito com sua consciência do que se não o fizer, sua atitude é, claramente, de solidariedade, é virtuosa e deve ser estimulada pelas instituições.   Já se essa mesma pessoa for obrigada pelo estado a destinar os mesmos dez por cento de sua riqueza para os necessitados, mesmo que isto não o deixe contrariado, não há aí qualquer indício de solidariedade nem de virtude, nem por parte dela nem por parte do estado: o que há, claramente, é um vício, que é o de se tentar fazer "solidariedade" à força e, ainda por cima, com o dinheiro dos outros.

 

                  Como observou com bastante clareza o presidente do Acton Institute, padre Robert Sirico (Toward a Free and Virtuous Society, Occasional Paper no. 9, Acton Institute for the  Study of Religion and Liberty, grand Rapids, Michigan, 1997), animais não podem ter comportamentos virtuosos, simplesmente porque lhes falta a faculdade da razão consciente.  Apenas a capacidade de reflexão e ação proposital que os humanos possuem é que pode habilitá-los a agir virtuosamente.  E o oposto é verdadeiro: ninguém pode ser acusado de agir viciosamente se não tiver capacidade de reflexão moral para efetivar as suas ações.  E, ainda: "Se agir com consciência moral significa praticar alguma virtude ou vício, então se deve pressupor a livre escolha.  Liberdade, portanto, está intimamente ligada à natureza da pessoa humana, uma vez que a livre escolha depende da razão humana.  Qualquer pessoa que falhe ao empregar sua capacidade de raciocinar doada por Deus está agindo abaixo de seu potencial humano" (ibid., p.2)

 

                  Portanto, não pode haver virtude onde não houver liberdade de escolha, onde não existir o direito de se fazer escolhas de natureza moral.  A liberdade é condição necessária para a virtude.

 

Não há liberdade sem virtude

 

                  A virtude reflete um padrão de moralidade intrapessoal e, como tal, é algo que não deve caber ao estado impor, mas a indivíduos e instituições não governamentais, particularmente as religiosas.  Na verdade, cada pessoa humana possui duas naturezas, a material e a espiritual.  Sob o ponto de vista teológico, a liberdade torna-se necessária porque cada pessoa tem um destino além da sociedade em que vive neste mundo e só pode realizá-lo sob condições de liberdade.  Isso significa que cada indivíduo é um fim em si mesmo e jamais deve ser tratado como simples meio para atingir-se determinado objetivo.  Tratá-lo assim constitui forte violação à sua natureza — ou, como diria Ortega y Gasset, à sua condição — e à sua dignidade.  E, além dessa violação à dignidade humana, quando uma sociedade trata seus cidadãos como meros meios, ela está minando os fundamentos morais da organização civil. 

 

                  Existe uma conexão entre liberdade econômica e liberdade pessoal e as supressões à liberdade econômica terminam suprimindo a liberdade pessoal, assim como o respeito à liberdade econômica gera mais liberdade pessoal.  Por exemplo, tanto faz você dizer a um pianista que ele pode tocar as canções que desejar, mas que ele será proibido de apresentar-se em público, ou então permitir que ele se apresente em qualquer lugar público, mas apenas tocando determinadas canções.

 

                  O processo de mercado, conforme definido pelo personalismo (individualismo metodológico) da Escola Austríaca, tem o efeito de funcionar como uma espécie de indutor ou, como diz Sirico, de "tutor moral" da sociedade, na medida em que consegue incentivar as regras de honestidade, de trabalho duro, de respeito para com os demais, de coragem, de busca, de iniciativa.   Para que possam funcionar adequadamente sob o ponto de vista moral, os mercados requerem um comportamento moral correto por parte de seus participantes.  Nenhum negócio pode sobreviver por longo tempo sem que os envolvidos nele tenham boa reputação, sem que exista confiança recíproca, civilidade, cortesia.  Se uma empresa, por exemplo, lesar sistematicamente seus consumidores, seus negócios acabarão diminuindo e, no longo prazo, desaparecerão.  Assim acontece nas economias livres, onde os consumidores têm liberdade de deixar de fazer negócios com as empresas que os lesarem. 

 

                  Além disso, em uma economia verdadeiramente de mercado, só serão bem sucedidos aqueles empresários que conseguirem satisfazer melhor os desejos dos consumidores, servindo-os e não os explorando ou os enganando.  É evidente que, como há pessoas que sucumbem à tendência original para o vício, sempre haverá inescrupulosos em qualquer atividade (não só nas econômicas, ressalte-se), mas para isso exatamente é que deve existir a lei.  Claramente, as instituições voluntárias ou ordens espontâneas, das quais o processo de mercado é um exemplo, são mais confiáveis na promoção das tradições, usos, costumes, ética e virtude do que o estado, que busca fazê-lo pela coerção.  Por isso, o cientista político e teólogo americano Michael Novak sugere que as sociedades devem buscar o fortalecimento das instituições que representem um meio termo entre o indivíduo e o estado, tais como as famílias, as associações de classes e de empresas e as igrejas, aquilo que ele chama de "economia intermediária".  Além disso, cremos que basta observarmos que apenas indivíduos — e não sociedades — podem ter virtudes.

 

                  Portanto, vemos que também é impossível haver liberdade onde não houver virtude, definida como escolhas morais corretas.  A virtude é condição necessária para a liberdade.   Uma depende da outra. 

 

III.   Princípios

 

            O quadro sinóptico seguinte ilustra o conjunto dos princípios, valores e instituições que, em seu conjunto, definem e estimulam o florescimento de uma sociedade — isto é, de um conjunto de indivíduos — livres e virtuosos.  Cada um dos atributos que o compõem guarda interdependência com os demais.

   

46.jpg

  

                  Quatro são os princípios que devem reger uma sociedade livre e virtuosa, a saber: o do respeito irrestrito à dignidade da pessoa humana, o do bem comum, o da solidariedade e o da subsidiariedade.  Tais princípios têm caráter geral e são basilares, uma vez que se referem à realidade social no seu conjunto: das relações entre os indivíduos àquelas que se desenvolvem na ação política, econômica e jurídica, bem como às que dizem respeito aos intercâmbios entre os diferentes povos e nações. 

 

                  A dignidade humana, a solidariedade, a subsidiariedade e o bem comum são princípios imutáveis no tempo e possuem um significado universal, o que os qualifica como parâmetros ideais de referência para a análise e interpretação dos fenômenos sociais, assim como para a orientação da ação humana no campo social, em uma perspectiva ampla, que Mises denominava de Praxeologia — o estudo da ação humana.  Os quatro princípios devem ser analisados em sua unidade, conexão e articulação, cada um deles requerendo a presença dos outros três para que se possa tentar promover uma articulação virtuosa da sociedade, mediante a qual cada consciência é instada a interagir com as demais, sob a égide da liberdade e em total coresponsabilidade com todos e em relação a todos.  Possuem um profundo significado moral, por nos remeterem aos próprios elementos ordenadores da vida em sociedade. 

  

(a)    a dignidade da pessoa humana

 

                  O objetivo central da Doutrina Social da Igreja — e que, infelizmente, os "teólogos da libertação" marxistas tentaram deturpar com frequência — sempre foi e ainda é a preservação da dignidade da pessoa humana e dos direitos inalienáveis do homem em sociedade, nas esferas da cultura, da economia, do Direito, da política e das relações internacionais.  Cada ser humano deve ser considerado fundamento, fim e sujeito de todas as instituições em que se expressa e realiza a vida social, na realidade daquilo que é e do que deve ser, segundo sua natureza intrinsecamente social e, no plano transcendental, da sua elevação ao seu destino final.   


            Essa centralidade da pessoa humana se fundamenta na antropologia cristã.  A dignidade humana não pode ser limitada por nenhum outro valor terreno, de ordem cultural, científica, política ou econômica, nem, evidentemente, a qualquer ideologia.  O respeito à dignidade da pessoa humana é uma condição necessária para o desenvolvimento das sociedades.  A concepção cristã de pessoa humana está necessariamente relacionada a uma justa visão da sociedade.  Desde a encíclica Rerum Novarum, o Magistério Romano tem ressaltado que a sociabilidade do homem não se esgota no estado, mas realiza-se em diversos corpos intermediários, desde a família até aos grupos econômicos, sociais, políticos e culturais.  Tais corpos intermediários são os principais garantidores da liberdade individual e do funcionamento das instituições políticas de uma sociedade, conforme demonstrou Alexis de Tocqueville (1805-1859) na clássica obra A democracia na América (1830 / 1835).  Tais corpos intermediários são provenientes da própria natureza humana, visto que é uma característica intrínseca do homem se relacionar com seu semelhante e formar diferentes tipos de comunidades, como sabiamente afirmou o monge trapista Thomas Merton (1915-1968): "nenhum homem é uma ilha".

  

(b)   o bem comum

     

                  Este princípio é decorrência direta da dignidade, unidade e igualdade de todas as pessoas humanas e, sem ele, é impossível a vida em sociedade encontrar pleno sentido.  O bem comum não é o simples somatório, de natureza holista, dos bens particulares; é de todos e de cada um — e, por isso, é comum — porque é indivisível e porque sua consecução, sua conservação e seu aumento exigem atuação conjunta.

 

                  A Gaudium et Spes, de 1966, define o bem comum como "o conjunto daquelas condições da vida social que permitem aos grupos e a cada um dos seus membros atingirem de maneira mais completa e desembaraçadamente a própria perfeição".  De fato, uma sociedade que pretenda estar a serviço do ser humano é aquela que estipula como meta prioritária o bem comum, entendido como bem de todos os homens e do homem como um todo.

 

                  É evidente que as exigências do bem comum variam conforme as condições sociais e econômicas de cada época e devem estar correlacionadas inexoravelmente com o respeito e a promoção integral da pessoa e de seus direitos fundamentais.  Exige o empenho de todos os participantes da sociedade e pressupõe que todos têm o direito de usufruir das condições sócio econômicas geradas por sua consecução.  O bem comum corresponde às mais elevadas inclinações da pessoa humana, mas é um objetivo bastante difícil de ser alcançado, porque exige a busca permanente do bem de outrem como se fosse o próprio. 

 

                  A responsabilidade por sua busca não cabe apenas às pessoas como indivíduos, mas também ao estado, já que o bem comum é a própria razão de ser da autoridade política.  Um governo ou, mais amplamente, um estado que não se preocupe com sua principal tarefa, que é a de buscar o bem comum, a rigor, não se justifica.  Para tentar assegurá-lo, o governo deve buscar harmonizar com base na justiça os diversos interesses setoriais, muitas vezes conflitantes.  Contudo, o bem comum não deve ser visto como um fim em si mesmo, porque seu valor está na busca pelos fins últimos da pessoa e aos de toda a criação.  Deus — ensina João Paulo II na Centesimus Annus (41) — "é o fim último de suas criaturas e por motivo algum se pode privar o bem comum da sua dimensão transcendente, que excede, mas também dá cumprimento à dimensão histórica".

 

                  Uma importante implicação do princípio do bem comum é o da destinação universal dos bens, que, concretamente, nos convida a cultivar noções da economia e da política inspiradas em valores morais que nos permitam agir sem jamais perder de vista a origem, a forma de produção e de obtenção e a finalidade dos bens econômicos e dos frutos das ações políticas.  A destinação universal dos bens encerra implicitamente a ideia de esforço comum, com os objetivos de estender a cada pessoa e a cada povo a possibilidade de desfrutar do desenvolvimento integral e de promover um mundo efetivamente humano, em que cada um possa simultaneamente dar e receber e onde o sucesso de uns não seja obstáculo ao sucesso de outros, nem pretexto para sua sujeição.  Como podemos depreender isto não tem nada em comum com as políticas assistencialistas tão comuns em alguns países.

      

(c)    a solidariedade

 

                  O processo de aceleração da interdependência entre os agentes econômicos que hoje presenciamos precisa ser acompanhado de atos concretos, na mesma velocidade, no plano da ética social, por parte dos indivíduos, do estado e dos chamados agentes intermediários, para que os avanços extraordinários experimentados pela atividade econômica não provoquem consequências indesejáveis, como, por exemplo, a exacerbação da distância que separa os ricos dos pobres, tanto em nível individual como no plano das nações, especialmente no provimento de igualdades de oportunidades.

 

                  Essa maior interdependência representa de fato várias formas de solidariedade e precisa transformar-se em relações que apontem para as exigências morais que devem enfeixar todas as relações humanas.  Lembremos que solidariedade é uma virtude moral e não um mero sentimento de compaixão pelos necessitados; é uma determinação firme e perseverante de empenho efetivo pelo bem comum e eleva-se ao grau de virtude social ao se colocar na dimensão da justiça, virtude dirigida essencialmente para o bem comum.  E mencionemos, ainda, que, como qualquer virtude, ela deve ser interior, ou seja, necessariamente voluntária.  Quando um governo tira de João para entregar a Pedro, isto não é solidariedade, é pura extorsão.

 

                  A solidariedade possui ligações bastante fortes com o bem comum, com a destinação universal dos bens, com a dignidade da pessoa humana, com a subsidiariedade e com a paz no mundo.   Implica assumirmos a dívida individual que temos para com a sociedade, já que nos beneficiamos diretamente de uma multiplicidade de condições proporcionadas por essa sociedade, que nos permitem existir, adquirir cultura, trabalhar, construir nosso patrimônio, enfim, de tudo o que a história da civilização nos ofereceu e continua a oferecer. 

  

(d)   a subisidiariedade

 

                  Se você tiver um problema com o seu vizinho de porta, o ideal é resolvê-lo sem recorrer ao síndico.  Se a questão é no condomínio, o correto é levá-la ao síndico e não à administração regional de seu bairro.  Se o problema for do bairro, recorra-se à administração e não à prefeitura.  Caso seja da cidade, para que recorrer ao governador, se existe a figura do prefeito, que ganha para isso?  Da mesma forma, se as dificuldades são em um estado, deve-se buscar o governador e não o presidente do país.  Estas regras básicas, que são respeitadas em todas as sociedades razoavelmente organizadas, compõem o princípio da subsidiariedade, a pedra angular do federalismo, da limitação do poder do estado e da liberdade individual. 

 

                  O referido princípio baseia-se na ideia de que é moralmente perigoso retirar-se a autoridade e a responsabilidade inerentes à pessoa humana, para entregá-la a um grupo, porque nada pode ser feito de melhor por uma organização maior e mais complexa do que pode ser conseguido pelas organizações ou indivíduos envolvidos diretamente com os problemas.  A subsidiariedade decorre de três importantes aspectos da própria existência humana. 

 

                  O primeiro é a dignidade da pessoa humana, que é decorrente do fato de termos sido criados à imagem e semelhança do Criador.  Assim, remover ou sufocar a responsabilidade e a autoridade individuais equivale a não reconhecer suas habilidades e sua dignidade.

 

                  O segundo é a complexa questão da limitação do conhecimento, soberbamente analisada por Hayek e outros estudiosos, especialmente os economistas da Escola Austríaca de Economia.  Como o conhecimento na sociedade é incompleto e apresenta-se sempre espalhado desigualmente, a negação do princípio da subsidiariedade, que ocorre quando as soluções dos problemas são passadas para o estado ou para organizações hierarquicamente superiores, na prática, acarreta uma ilusão de ótica, uma crença em um "olho central" que pode enxergar todas as coisas, conhecer todas as necessidades e demandas individuais, regular os setores envolvidos a contento e solucioná-las da forma socialmente correta.  Ora, o planejamento central sempre fracassou e haverá de fracassar exatamente porque esse "olho" não apenas não existe, mas principalmente porque jamais poderá existir. 

 

                  Por fim, o terceiro aspecto que justifica a prática da subsidiariedade é a solidariedade com os pobres e menos favorecidos, simplesmente porque essas pessoas são mais do que meramente a sua própria pobreza, por espelharem a imagem divina e a dignidade disto decorrente, a despeito de suas carências materiais.  Os programas governamentais de transferências de rendas, mesmo quando bem intencionados e bem gerenciados, só são capazes de enxergar as necessidades materiais.  Além disso, os engarrafamentos quilométricos provocados pela burocracia, somados à insuficiência de conhecimento total dos problemas, impedem esses programas de atenderem a todas as necessidades das pessoas humanas.  Como a pobreza manifesta-se de várias formas, bastante complexas e às vezes muito distantes da mera falta de bens materiais, quem vive mais perto dos necessitados está necessariamente melhor posicionado, em termos de conhecimento, não apenas para ajudar a resolver as necessidades materiais, mas para dar um tratamento mais adequado às demais. 

 

                  Nas palavras de Madre Teresa de Calcutá, solidariedade significa que "o rico salve o pobre e o pobre salve o rico", uma vez que ambos tendem a ganhar com sua interação.  A erradicação da miséria e o alívio da pobreza, em sua forma correta, não são unidirecionais, porque levam ambos — o que doa e o que recebe — a serem abençoados.

 

                  Tais reflexões parecem-nos particularmente importantes, especialmente em países em que prevalece o péssimo hábito — secular e cultural — de cultivar a centralização política, econômica e administrativa. 

 
 

III.  Valores

  

                  Existe uma relação de reciprocidade entre os quatro princípios que acabamos de expor e os valores, uma vez que estes expressam o apreço que se deve guardar para com diversos aspectos do bem moral que os princípios objetivam alcançar, servindo como ponto de referência para a estruturação e a ordenação da vida social.  Os valores sociais básicos são três, todos inerentes ao princípio da dignidade da pessoa humana, da qual representam o que em economia chamamos de variável instrumental e são: a verdade, a liberdade, e a justiça.

  

(a)    a verdade

 

                  Em nossa tradição judaico-cristã todos os homens estão obrigados, desde Moisés, a tender continuamente para a verdade, a respeitá-la e a dela dar testemunho de modo responsável.  Viver na verdade tem um significado bastante especial nas relações sociais, porque ordena e alimenta a convivência entre as pessoas e povos, de forma condizente com a dignidade pessoal. 

 

                  Os dias atuais, claramente, exigem de cada um de nós um enorme esforço educativo — podemos dizer, mesmo, um gigantesco empenho —, no sentido de promover a busca da verdade em todos os âmbitos e de sobrepô-la às inúmeras tentativas de relativizar suas exigências e de tentar desmoralizá-la com base em pseudoargumentos vestidos com a fantasia da "modernidade" ou com meros xingamentos do tipo "falso moralismo". 

 

                  É dever de todas as pessoas de bem, religiosas ou não, inclusive para que possamos preservar nossa própria dignidade, lutar pela busca da verdade, seja no plano da verdade revelada, seja na cultura, na ciência, na economia, na política ou quem qualquer outro ramo das atividades humanas.

  

(b)    a liberdade

 

                  Já escrevia São Paulo aos coríntios: "Ubi autem Spiritus Domini ibi libertas".  A liberdade da pessoa humana é um sinal claro da imagem do Criador e, por conseguinte, sinal de sua dignidade.  O valor da liberdade, como expressão da singularidade de cada ser humano, é respeitado na medida em que se consente a cada membro de uma sociedade realizar sua própria vocação individual, mediante suas próprias escolhas ao longo da vida. 

 

                  Na seção anterior, discutimos os diversos conceitos de liberdade e mostramos que liberdade e virtude são indissociáveis. 

  

(c) a justiça

  

                  Subjetivamente, a justiça se traduz na atitude, determinada pela vontade livre, de reconhecer o outro como pessoa e, objetivamente, no critério determinante da moralidade no âmbito intersubjetivo e social.  As formas clássicas de justiça são a comutativa, a distributiva e a legal.  No que se refere à última, no entanto, há que se fazer uma distinção entre a lei e o direito, uma vez que nem tudo o que é legal é necessariamente justo.  Na antropologia cristã, a justiça, na verdade, não é uma simples convenção humana (thesis, na linguagem de Hayek), pois aquilo que é "justo" não é determinado por qualquer lei ou legislação formal, mas sim pela identidade profunda do ser humano (cf. Sollicitudo Rei Socialis, 39).

 

                  No mundo de nossos dias, a importância da justiça parece maior, porque a sociedade moderna vem mostrando uma tendência a ameaçar o valor, a dignidade e os direitos da pessoa humana, mesmo disfarçando tal inclinação sob proclamações de intentos aparentemente "justos", na medida em que tende a valorizar exclusivamente os critérios de utilidade e de posse.  Tal perigo tem se manifestado, entre outros canais, pela chamada "doutrina do direito alternativo", que dá aos magistrados o poder de, em nome do conceito de "justiça" que lhes seja conveniente, promover a "justiça social". 

 

                  É preocupante quando uma doutrina sustenta que um juiz está acima da lei, submetendo-a a suas preferências ideológicas ou partidárias individuais, sob o pretexto de que seria dever do direito realizar "transformações sociais", uma vez que a lei seria produzida pelos que estão no poder e, portanto, refletiria os interesses da classe dominante (burguesia), em detrimento do "proletariado".  A "doutrina do direito alternativo", também denominado de "direito paralelo" e "direito insurgente", repudia os princípios consagrados de neutralidade da lei e de imparcialidade do juiz.  A lei não seria neutra porque se origina do poder dominante e o juiz não deveria ser imparcial porque deve julgar os fatos subjetivamente e posicionar-se tendo em vista objetivos "sociais" (ou seja, "revolucionários"), o que lhe aumenta os poderes e lhe permite questionar o conjunto de normais legais vigentes.  O magistrado entra dessa forma diretamente na "luta de classes", abandonando sua postura de imparcialidade, que o "aprisionaria" dentro do estrito cumprimento da lei. 

 

                  É uma visão ideológica do direito, supralegal e inteiramente comprometida com o socialismo distributivista, além de incompatível com a garantia das liberdades individuais.  Primeiro, porque ao enfeixar o conceito marxista de "lutas de classes", retira do direito o seu atributo de ciência normativa.  Segundo, porque o juiz não pode substituir o legislador.  Terceiro, porque se uma determinada lei é "injusta", o correto é que o legislativo a revogue e não que o juiz a modifique de acordo com o que pensa com os seus botões.  Quarto, porque defender que juízes não sejam imparciais é uma agressão ao bom senso.  Quinto, porque lhes confere poderes exorbitantes, dotando-os de um livre arbítrio que pode ser calamitoso.  Sexto, como cada cabeça é uma sentença, abre as portas para jurisprudências contraditórias, ou seja, para a insegurança jurídica.  Sétimo, nega o princípio do devido processo legal, ou seja, a garantia de que ninguém pode ser atingido em seus bens e direitos sem o competente processo legal que respeite princípios constitucionais diretivos, como o da legalidade, o da isonomia e o do contraditório. 

       

IV.  Instituições

      

                  Três são as instituições básicas de uma sociedade virtuosa, a saber: o estado de direito, a economia de mercado e a democracia representativa.  Estes três pilares, a partir do final dos anos 80, vêm, felizmente, se transformando em consenso no mundo ocidental, embora alguns países, em especial algumas repúblicas da América Latina, venham dando preocupantes sinais de recuos. 

  

(a)    o Estado de Direito

 

                  Em artigo que está na página do Centro Interdisciplinar de Ética e Economia Personalista (CIEEP) na internet (www.cieep.org.br), Alex Catharino de Souza mostra que o grande desafio da modernidade no campo político é criar regras jurídicas que ao mesmo tempo garantam a autonomia dos indivíduos e limitem a possibilidade de danos perpetrados por terceiros.  O ordenamento da sociedade segundo três poderes — legislativo, executivo e judiciário — reflete uma visão realista da natureza social do homem, a qual exige uma legislação adequada para proteger a liberdade de todos.  Para tal fim é preferível que cada poder seja equilibrado por outros poderes e outras esferas de competência que o mantenham no seu justo limite.  Este é o princípio do Estado de direito, no qual é soberana a lei, definida como um conjunto de normais gerais, abstratas e prospectivas de conduta, e não a vontade de um indivíduo ou de grupos esparsos de indivíduos.  Aliás, a necessidade de regras de justa conduta antecede, na história da civilização, a própria necessidade de um ente para formulá-las e as fazer respeitar, ou seja, o estado.


            O estado de direito é uma solução prática para minimizar os riscos de o estado destruir a liberdade de consciência e de ação da pessoa humana.  O objetivo do estado de direito é restringir, tanto quanto possível, as tarefas e os poderes do setor público e do processo político, ampliando ao máximo possível a liberdade individual.

 

            É a instituição mais adequada para a implantação da justiça e a única forma de organização jurídica capaz de permitir a convivência livre e harmoniosa das pessoas humanas.  Todavia, conforme ressalvamos anteriormente, não é uma mera norma legal, mas uma complexa doutrina metalegal e um profundo ideal político que diz respeito àquilo que o estado e as leis devem ser.  No plano prático ele pode ser entendido como o "império da lei", a autoridade da lei em lugar da lei da autoridade, a rigorosa delimitação constitucional dos poderes públicos, a submissão da lei ao princípio da "isonomia" e à eficácia da justiça. 

 

            Dentre as características fundamentais do estado de direito podemos destacar treze princípios, a saber: (1) supremacia da lei; (2) isonomia; (3) ausência de privilégios;
(4) respeito aos direitos individuais; (5) aplicação da justiça; (6) promoção e não transferência da responsabilidade individual; (7) existência de salvaguardas processuais; (8) limitação do poder discricionário; (9) respeito às minorias; (10) constitucionalismo; (11) divisão horizontal dos poderes estatais; (12) divisão vertical dos poderes estatais (de acordo com o princípio da subsidiariedade e (13) garantia da liberdade de entrada nos mercados.


            O estado de direito é a condição necessária para o bom funcionamento da economia de mercado e para a instauração de uma democracia representativa que não se torne despótica.  Entretanto, o estado de direito não pode ser mantido sem realmente assegurar a efetividade da lei.  Logo, a compreensão do verdadeiro significado do estado de direito exige o conhecimento doo sentido preciso que damos ao conceito de lei. 



(b) a economia de mercado

                 

                  A abordagem austríaca da ordem de mercado pode ser sintetizada na firme posição de Hayek de que não existe uma economia, mas um sistema extremamente complexo formado por miríades de economias interconectadas, que são as famílias, as empresas e os negócios em geral e que a ciência que estuda essas interconexões é a cataláctica (ou cataláxia), que procura analisar as ordens espontâneas produzidas pelo mercado mediante as ações dos indivíduos e baseadas em normas de direito de propriedade, de respeito aos contratos e de obrigações.  Uma das características essenciais de uma economia de mercado é a descoberta permanente que proporciona aos participantes, dado que o que constitui o processo de mercado é a ação humana, ao longo do tempo (real ou dinâmico), de milhões de indivíduos (que nem se conhecem), sob condições de incerteza genuína, ou seja, não probabilística.

 

                  A relação entre moral e economia é necessária e intrínseca: atividade econômica e comportamento moral se complementam intimamente e a distinção entre ambas não significa separação entre os seus âmbitos, mas uma importantíssima reciprocidade.  A dimensão moral da economia transforma em finalidades indivisíveis, não separadas e não alternativas a eficiência econômica e a promoção de um desenvolvimento solidário.  João Paulo II nos deixou uma defesa clara e veemente da economia de mercado na Centesimus Annus:

 

                  "A atividade econômica, em particular a da economia de mercado, não se pode realizar num vazio institucional, jurídico e político.  Pelo contrário, supõe segurança no referente às garantias da liberdade individual e da propriedade, além de uma moeda estável e serviços públicos eficientes.  A principal tarefa do estado é, portanto, a de garantir esta segurança, de modo que quem trabalha e produz possa gozar dos frutos do próprio trabalho e, consequentemente, sinta-se estimulado a cumpri-lo com eficiência e honestidade.  A falta de segurança, acompanhada pela corrupção dos poderes públicos e pela difusão de fontes impróprias de enriquecimento e de lucros fáceis fundados em atividades ilegais ou puramente especulativas é um dos obstáculos principais ao desenvolvimento e à ordem econômica" (João Paulo II, Carta Encíclica Centesimus Annus, Loyola, São Paulo, 1991, nº 48, pág.  65).

 

                  Na vida econômica em particular e na vida humana em geral, a primazia da moral é uma lei demonstrável e fundamental para a prosperidade, é um princípio filosófico e empírico que não pode ser violado.  Quando isso ocorre, surgem os vícios morais, como a preguiça, a desonestidade, a corrupção, a coerção, a avareza, a apropriação do estado por máquinas partidárias e tantos outros que, como traças, corroem pouco a pouco a sociedade.   

  

(b)    a democracia representativa

 

                  Uma verdadeira democracia não se restringe ao mero respeito formal a certas regras nem à "vontade da maioria", mas deve significar o resultado da aceitação dos valores inspiradores dos sentimentos democráticos mais autênticos: a dignidade da pessoa humana, o respeito aos direitos individuais e a assunção do bem comum como fim e critério regente da vida política.  Um dos maiores riscos para as atuais democracias consiste no relativismo ético, que induz subjetivamente a negar a existência de critérios objetivos e universais para estabelecer o fundamento e a reta hierarquia de valores.  Ora, ao negarmos a existência de uma verdade consagrada durante muitos séculos na civilização ocidental — a tradição judaico-cristã — para guiar e orientar a ação humana no campo político, então as ideias e as convicções podem ser facilmente instrumentalizadas e utilizadas para fins de poder.  Uma democracia sem valores morais sólidos que a fundamentem pode facilmente converter-se em um totalitarismo aberto ou disfarçado, como a história é farta em demonstrar. 

 

                  A democracia — como ensina a filosofia política da Escola Austríaca — é um ordenamento da ordem política e social, certamente aquele com menos defeitos que os homens descobriram no processo evolutivo das ordens espontâneas no plano político.  Mas ela não pode e não deve ser vista como um fim mesmo, mas como um meio, um instrumento a serviço da dignidade da pessoa humana e do bem comum.  Seu conteúdo moral não é automático, porque depende da sua conformidade ou de sua falta de conformidade com a lei moral, à qual deve estar submetida como, a rigor, qualquer outra forma de ação humana.  Isto significa que a democracia depende da moralidade dos fins que busca e dos meios que utiliza para atingi-los. 

 

                  Uma democracia representativa, no âmbito de uma sociedade virtuosa, deve atender a diversos requisitos, dentre os quais destacamos:

 

(1) o empenho dos eleitos na busca do bem comum;

(2) a dimensão moral da representação;

(3) o fato de que o estado existe para servir aos cidadãos (e não para servir-se deles);

(4) a punição a qualquer forma de corrupção política, uma das mais graves deformações do sistema democrático, porque ela agride a um só tempo os princípios da moral e as normas da justiça;

(5) o acesso de todos às responsabilidades públicas;

(6) o respeito total à liberdade de informação, em todos os setores da vida social, a saber, o econômico, o político, o cultural, o educativo e o religioso;

(7) o respeito à vontade da maioria, mas com o resguardo da garantia, também, dos direitos da minoria;

(8) a não ideologização do estado e o seu não aparelhamento por representantes do partido que eventualmente ocupa o poder;

(9) a descentralização do poder (subisidiariedade).

  

VI.  Conclusões

 

                  O homem que age e reage no campo moral é o mesmo homem racional e volitivo que age e reage nos campos da economia, da política e da religião, é o mesmo que age e reage nos campos cultural, esportivo e das artes.  A vida de todos nós, — quer o queiramos ou não —, está condicionada à economia que, por sua vez, está condicionada à quantidade e à qualidade dos bens produzidos, os quais, por sua vez, dependem da atividade produtiva, que é o campo cheio de vida e animação em que deve florescer a liberdade interior dos indivíduos, das associações de indivíduos e das relações entre indivíduos, relações essas que se constituem na fonte da responsabilidade e, portanto, da moralidade das ações humanas, da virtude e do vício que existem no mundo.

 

                  Nas obras de muitos pensadores em sintonia com a economia personalista da Escola Austríaca, percebe-se que não é possível defender a tese que, de tempos em tempos, parece voltar à moda, aquela de uma terceira via ou caminho, intermediário entre o capitalismo e o socialismo.  Pelo contrário, nota-se que, sendo o capitalismo uma força natural da história, porque, apesar de seus defeitos — frutos dos vícios humanos, que também continuam a existir, só que, nos sistemas centralizados, de maneira potencializada — ele mostrou claramente ser o sistema que, mediante a livre iniciativa econômica, apresenta maior capacidade de mobilizar virtudes e restringir vícios da melhor forma possível.  Trata-se não de encontrar uma terceira via, mas de preocupar-se em proporcionar ao capitalismo, dentro do espírito da liberdade integral e indivisível, a inspiração moral correta.

 

                  Uma visão da ação humana assim concebida pode ser capaz de formar uma novus ordo saeculorum, em cujo centro deve estar a liberdade integral e indivisível: a democracia ou liberdade política, a economia de mercado ou liberdade econômica e o pluralismo ou liberdade religiosa e de opinião, ressaltando-se que esta última é a primeira das liberdades, porque é a fonte e a síntese das outras duas.

 

                  O personalismo metodológico, a liberdade integral e indivisível, a falibilidade social que requer os três poderes e a subjetividade criativa da mente humana são elementos suficientes para a defesa de uma práxis política objetiva, em cujo centro está a primazia da pessoa humana e que se coloca como diametralmente oposta a qualquer forma de planejamento central, de monopólio do estado, de intervencionismo e dirigismo econômico, de ceticismo e de relativismo moral.

 

                  Um problema prático importante é: como proporcionar à economia personalista, dentro do princípio universal da liberdade integral e indivisível, as inspirações morais corretas, de acordo com a tradição judaico-cristã?  A resposta mais sugestiva parece ser a adoção do denominado princípio de subsidiariedade.  Esse importantíssimo princípio — que está em perfeita harmonia com a tradição da economia personalista, com a Escola Austríaca, e com a Doutrina Social reta e, portanto, com os fundamentos teóricos que comentamos anteriormente — exorta o estado a abster-se de intervir em todas as questões em que seja demonstrado que os indivíduos, as famílias, os grupos intermediários e as associações possam apresentar soluções superiores no sentido de proverem de modo autônomo as próprias necessidades, tais como escolas livres, empresas privadas, bancos e inúmeras outras instituições econômicas, sob pena de provocar um dano grave e uma deformação na ordem social moralmente correta.

 

                  Os erros fundamentais — o fatal conceit hayekiano — do estatismo são o de atribuir as atividades produtivas ao estado, em um sistema econômico que sufoca a liberdade de iniciativa privada e o de transferir o capital privado ao estado, fazendo-o operar em setores predefinidos da economia, mediante as chamadas "políticas públicas".  A iniciativa privada não precisa esperar pelas benesses que caem do alto como o maná, distribuídas pelos políticos e burocratas — sejam eles democráticos ou totalitários —, pois ela, movida por uma fundamentação moral mais sólida, como analisamos, tem muito mais condições de proporcionar padrões de resultados econômicos mais conformes à dignidade humana do que as ações estatistas mascaradas pelo rótulo politicamente correto de "políticas públicas".

 

                  Como afirmou Luigi Sturzo, referindo-se à questão da educação na Itália:

 

                  "Finché la scuola in Italia non sarà libera, neppure gli italiani saranno liberi; essi saranno servi, servi dello Stato, del partito, delle organizzazioni private o pubbliche di ogni genere [...] La scuola vera, libera, gioiosa, piena di entusiasmi giovanili, sviluppata in um ambiente adatto, com insegnanti impegnati nella nobile funzione di educatori, non può germogliare nell'atmosfera pesante creata dal monopolio burocratico statale".   ("Se as escolas na Itália não são livres, os italianos também não o serão; eles serão servos, servos do estado, do partido, das organizações privadas ou públicas de todos os gêneros {...} A escola verdadeira, livre, alegre, cheia de entusiasmo juvenil, desenvolvida em um ambiente adequado, com professores empenhados na função nobre de educadores, não pode germinar na atmosfera pesada criada pelo monopólio burocrático estatal", tradução nossa).   

 

                  O raciocínio é rigorosamente o mesmo se, ao invés de escolas, pensarmos em fazendas, indústrias, escritórios, previdência, saúde ou qualquer outra atividade produtiva.

 

                  Na base do princípio da subsidiariedade repousa uma certeza, a de que entre o estado frio e impessoal e o indivíduo abandonado a si mesmo, existe uma primeira linha de defesa, que é encontrada nos agentes intermediários, nas pequenas células, nos bolsões vitais, como soem ser a família, as empresas, os negócios, as escolas, as associações, as igrejas, cujo próprio agir é indispensável para um desenvolvimento equilibrado da pessoa humana e para uma melhor organização política, econômica e cultural, fundamentada nas noções de liberdade integral e indivisível e de justiça social, esta última entendida como a existência das condições que favorecem a dignidade da pessoa humana e não como o conceito meramente redistributivo de inspiração socialista, tão corretamente atacado por Mises, Hayek e os demais austríacos.

 

                  Uma interessante aplicação do importante princípio da subsidiariedade e que vem representando uma resposta bastante satisfatória ao aparente conflito entre a conciliação de interesses sociais e interesses individuais, em conformidade com a tradição da economia personalista dos austríacos e com a teoria do livre mercado derivada das encíclicas de João Paulo II é a experiência com as denominadas "economias alternativas", que começaram a germinar a partir do início dos anos noventa no mundo católico.  Entre essas experiências, devemos ressaltar a do Movimento dos Focolares, criado por Chiara Lubich, chamada de economia de comunhão, que vem se estendendo por todo o mundo, inclusive no Brasil.  Trata-se de uma experiência com um estilo econômico particular que, sob o aspecto da organização da produção, procura reunir o respeito às regras e valores existentes dentro de cada empresa com outros valores, motivações e objetivos, que podem ser resumidos na expressão cultura da comunhão na liberdade.   A economia da comunhão não propugna uma nova forma de empresa, diferente das já existentes, nem muito menos perseguir as empresas privadas ou obrigá-las a praticar "solidariedade" à força, mas sim a transformação de dentro para fora das estruturas das empresas, a partir de cada um de seus membros individuais, lançando sobre todos os seus negócios um sentido de vida de comunhão, sempre respeitando os padrões e os valores próprios de cada empresa e do processo de mercado, tão evidenciados por João Paulo II e pelos economistas austríacos. 

 

                  Como vemos, é um conceito muito mais adequado do que o de "responsabilidade social" das empresas que expressa, no fundo, uma atitude de repúdio aos valores da economia de mercado.  De fato, uma empresa, quando é aberta, já começa a desempenhar, com sua simples produção, uma função social (sem aspas), uma vez que ela está inserida na sociedade.

 

                  Depois de tudo o que escrevemos, podemos concluir afirmando que parecem ser bastante sólidos os liames existentes entre a tradição judaico-cristã, especialmente a formalizada na Doutrina Social da Igreja e a tradição do liberalismo clássico personalista, em especial aquele da Escola Austríaca de Economia. 

 

                  Em resumo: Civitas propter cives, non cives propter civitatem! (a cidade deve existir para os cidadãos e não estes para as cidades).  

                  