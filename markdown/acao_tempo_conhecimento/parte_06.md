## Capítulo 6 - MOEDA, CAPITAL E TEMPO NA TEORIA MACROECONÔMICA

I.  Introdução

 

 

                     Este capítulo objetiva enfatizar a importância do capital, da moeda e do tempo na teoria econômica e, mais especificamente, na macroeconomia.  Desde a Teoria Geral de Keynes, formulada nos anos 30 do século passado, os economistas, infelizmente, abandonaram quase completamente a teoria do capital, sem colocar outra teoria para substituí-la.  Com efeito, os modelos ditos "macroeconômicos", sejam de viés keynesiano ou neoclássico, tendem a enfatizar que no curto prazo o estoque de capital de uma economia é constante, o que os levou ao abandono mencionado.  Há, além disso, concepções distintas sobre o papel da moeda e do fator tempo nessas teorias.  Isto tudo somado, ou seja, o abandono da teoria do capital, as visões diferentes da moeda e as formas como é introduzido o fator tempo, permite-nos estabelecer comparações teóricas bastante interessantes entre a mainstream economics (keynesianos e neoclássicos) e a Escola Austríaca de Economia, que sempre deu importância à teoria do capital e que possui concepções diferentes a respeito de como a moeda e o tempo devem ser tratados teoricamente.

 

                     As idéias aqui expostas não são de nossa autoria, embora as endosse integralmente, mas do Professor Roger Garrison, da Universidade de Auburn, nos Estados Unidos, um economista austríaco bastante criativo e que tem se dedicado a estudar a Teoria Austríaca dos Ciclos Econômicos, modernizando-a e procurando compará-la com as diferentes visões da mainstream economics.  Neste capítulo, apenas as explicamos e fazemos alguns comentários adicionais.

 

                     Garrison observa que podemos de início ancorar as proposições macroeconômicas no binômio tempo/moeda.  As doenças macroeconômicas originam-se nas formas especiais de interação, em uma economia de mercado, desses dois elementos, que constituem a universalidade, ou os denominadores comuns da teoria macroeconômica.  O reconhecimento explícito desse universo proporciona uma comparação bastante rica entre keynesianos e monetaristas: os primeiros negam a possibilidade de soluções de mercado para as doenças macroeconômicas, enquanto os segundos tendem a negar as próprias doenças.  A Teoria Austríaca situa-se entre esses dois extremos.

 

 

 

II.  O universo da teoria macroeconômica

 

 

                     Existe um "mercado de tempo" e um "mercado de moeda" e esses dois mercados, tanto se forem isolados conceitualmente ou interagindo no mundo real, é que dão origem a todos os fenômenos que se convencionou chamar de "macroeconômicos".  A Escola Austríaca é a única a reconhecer explicitamente a natureza universal do tempo na teoria econômica, ao propor que todas as escolhas são feitas com um olho no futuro e levando em conta uma memória que as liga ao passado e todas as ações humanas são escolhas levadas a efeito no tempo e em ambiente de incerteza genuína, em que os mercados são considerados como processos de permanentes descobertas, em procedimentos de erros e tentativas.

 

                     Analisar uma economia de mercado nada mais é do que identificar ações individuais que originam os fenômenos de mercado, mas sabendo que o tempo, literalmente, é o meio através do qual essas ações transpiram.  A prática comum na microeconomia, particularmente nas teorias walrasianas de equilíbrio geral, de limitar os problemas a casos de economias de trocas, não passa de uma tentativa de abstração do elemento tempo.  O sabor macroeconômico distingue-se da análise de equilíbrio geral pela atenção que procura dispensar ao fator tempo, associando-o com decisões de produção.  Essas considerações com respeito ao tempo são feitas, na tradição de Chicago, por meio dos conceitos de custos de informação; na tradição keynesiana, por meio dos conceitos de viscosidade e de rigidez e, na tradição austríaca iniciada por Carl Menger, pelo conceito de estrutura de capital.    As teorias macroeconômicas alternativas, cada uma à sua maneira, consistem em tentativas de explicação do comportamento dos indivíduos em seus intuitos de, como o próprio Keynes escreveu, "... defeat the dark forces of time and ignorance which envelope our future".

 

                     Tal como no tratamento do fator tempo, a forma como a moeda é introduzida nas diferentes teorias, seja como um ativo financeiro, seja como um hedge contra taxas de juros que tendem a subir, seja como um meio de troca, explica boa parte das principais diferenças entre as concepções alternativas.  Tempo é meio de ação; moeda é meio de troca.  Os dois, em conjunto, servem para definir a análise macroeconômica.  Se as trocas intertemporais e interpessoais pudessem ser isoladas em uma economia de mercado, a macroeconomia convencional, como sugere o Professor Garrison, seria fortemente redundante e nesse caso a teoria do capital de Cambridge (que se abstrai das trocas interpessoais) e a teoria do equilíbrio geral walrasiana (que se abstrai das trocas intertemporais), resolveriam adequadamente os principais problemas.   Mas é precisamente essa "interseção" dos "mercados" de tempo e de moeda que se constitui na principal questão macroeconômica.

 

 

 

 

III.  Capital e tempo

 

 

 

                     William Stanley Jevons já afirmava que uma das maneiras de se concretizar a noção de "mercado de tempo" consiste em reconhecer o aspecto temporal essencial dos mercados de bens de capital em sua concepção mais ampla.  Menger, como sabemos, introduziu em seu livro seminal, que deu origem à Escola Austríaca, a idéia de bens de diversas ordens, em que ordem denota uma relação temporal entre um bem de capital e o eventual bem de consumo que esse bem de capital contribui para produzir.  Por exemplo, se o pão é um bem de consumo e a farinha um bem utilizado na fabricação de pães, então o primeiro é considerado um bem de primeira ordem e a segunda um bem de segunda ordem, enquanto o trigo, usado para fabricar a farinha, o bem de terceira ordem.

 

                     Seria bem interessante se pudéssemos comparar a noção de capital da Escola Austríaca com alguma visão alternativa formulada por Keynes, mas isto não é possível, porque, apesar de haver repudiado a contribuição de Böhm-Bawerk, Keynes não a substituiu por nenhuma outra.  Assim, o keynesianismo representa um perigoso desligamento do pensamento macroeconômico das importantes questões relacionadas com a Teoria do Capital e representa também, infelizmente, o abandono direto das abordagens que têm o objetivo de lidar com o fator tempo, caracterizadas por aquela famosa frase de que "no longo prazo estaremos todos mortos" - uma afirmativa bastante infeliz e que produziu efeitos desastrosos na teoria econômica e na prática. 

 

                     É claro que sua teoria requer algum tipo de especulação em relação ao futuro, mas, quando o "espírito animal" dos investidores keynesianos é colocado diante das "forças obscuras do tempo e da ignorância", estas parecem vencer sempre.  Esta característica da teoria keynesiana (e da Macroeconomia em geral), deriva diretamente do abandono da teoria do capital utilizada antes de Keynes para incorporar o "mercado de tempo".  Vejamos, agora, o papel da moeda para os austríacos, os keynesianos e os monetaristas (novos clássicos). 

 

 

 

IV.  A moeda como uma junta frouxa (loose joint)

 

 

                     As forças intertemporais de mercado encontram sua expressão mais direta e concreta nos mercados de bens de capital.  Se estes bens fossem trocados diretamente por bens de consumo ou por outros bens de capital, a natureza da macroeconomia seria substancialmente diferente.  Mas o fato de que os bens de capital e os bens correspondentes de consumo são trocados indiretamente via moeda é que adiciona a outra dimensão essencial.  A teoria macroeconômica, então, deve analisar as implicações das trocas indiretas no contexto de uma economia que utiliza capital.  Em sua The Pure Theory of Capital, Hayek concebe a moeda como uma junta frouxa (loose joint) dentro de um sistema de mercado que se autoequilibra.  A moeda é a junta que liga a capacidade de demanda com o desejo de produzir, o que dá margem para o entendimento correto da lei de Say.  Dizer que a moeda é uma junta "frouxa" em uma economia que utiliza capital nos lembra que existe um lapso de tempo entre a oferta de um dado sortimento de bens de capital e a demanda subseqüente de bens de consumo.  E é esse time lag que provoca os problemas macroeconômicos mais comuns, tais como o conhecido como "sobreinvestimento", que os austríacos chamam de "maus investimentos" ("malinvestments").

 

                     Essa imagem de Hayek nos permite testar a adequação das diversas teorias em que a moeda desempenha um papel importante.  A concepção da moeda como uma junta frouxa sugere que devemos evitar duas construções teóricas polares.  Introduzir a moeda como uma junta apertada (tight joiint) é negar o problema da coordenação intertemporal.  Tais modelos simplesmente colocam todas as trocas, sejam  intertemporais  ou  não, em um contexto de equilíbrio geral atemporal.  Por outro lado, introduzir a moeda como uma junta quebrada (broken joint) é negar a própria possibilidade de uma solução de mercado para os problemas de coordenação intertemporal.

 

                     Em um mundo em que a moeda fosse uma junta quebrada, os preços não transmitiriam as informações sobre as alocações de recursos desejadas ao longo do tempo, nem tampouco ajudariam a atualizar essas alocações.  Em outras palavras, os conceitos de moeda como junta apertada e junta quebrada negam, respectivamente, o problema macroeconômico central e a sua solução.  A macroeconomia da mainstream, a saber, o monetarismo e o keynesianismo, tende a adotar uma das duas posições polares e o resultado é que, como uma primeira aproximação, os problemas macroeconômicos ou são vistos como triviais ou, então, como insolúveis.  Entre essas duas concepções extremas é que está a noção hayekiana da moeda como uma junta frouxa, que nos conduz a reconhecer os problemas e a deixar a possibilidade de soluções de mercado para eles como uma questão em aberto. 

 

                     Notemos, contudo, que a hipótese de que a moeda é uma junta apertada não pode ser condenada em todos os contextos.   Ela nos conduz, por exemplo, às noções da moeda como um "véu", bem como ao núcleo de verdade contido na teoria quantitativa da moeda, o de que a inflação é um fenômeno monetário.  Mas, ao mesmo tempo, não podemos nos esquecer da excessiva simplicidade, face ao mundo real, dessa hipótese.

 

 

 

V.  A macroeconomia da mainstream

 

 

                     (a) Embora Wicksell seja geralmente considerado como tendo sido o primeiro a integrar a teoria monetária com a teoria do valor, na realidade ele não fez isso.   Em seu modelo, os preços, puxados pelo efeito dos saldos reais, movem-se todos simultaneamente, seja para cima ou para baixo.   Mesmo quando é reconhecido que alguns preços podem variar (temporariamente) de maneiras diferentes entre si, o modelo wickselliano não faz menção a variações correspondentes nas quantidades.  Com isso, não explica as interações entre o mercado de moeda e os mercados de bens de capital, focalizando apenas as relações entre a quantidade total de moeda e o nível geral de preços.

 

                     (b) Considerações monetárias e de valor costumam ser segregadas pela hipótese implícita ou explícita de que a moeda é uma junta apertada.  E é essa segregação entre as teorias monetária e do valor que costuma levar à substituição da estrutura mengeriana de produção por algum agregado (PIB), o que significa admitir que, dado o caráter de tight joint da moeda, nada de relevante se passa no setor real da economia.


                     (c) Keynes, por sua vez, descartou a teoria do capital de Böhm-Bawerk sem substituí-la por qualquer outra.  Assim, seria apenas por "acidente ou desígnio" (Teoria Geral, ed. de 1964, p. 28), em oposição a uma ordem espontânea, que a economia poderia atingir a coordenação macroeconômica.    Ou seja, com a hipótese de que a moeda se constitui em uma junta quebrada, Keynes e os keynesianos de diversos matizes sempre acabam descartando a solução de mercado para os problemas macroeconômicos.

 

                     (d) Sob essa perspectiva, a alternativa à teoria macroeconômica oferecida por Mises, Hayek e por outros austríacos representa um meio termo, ao mesmo tempo em que é radical, no sentido de ir diretamente à raiz dos problemas.

 

 

 

 

VI.  A teoria "macroeconômica" austríaca

 

 

 

                     Embora os economistas austríacos rejeitem a macroeconomia, podemos nos valer deste termo para efeitos de comparação com as teorias macroeconômicas conhecidas.  Entretanto, é conveniente que o usemos sempre entre aspas.

 

                     O ponto significativo da Escola Austríaca que ressalta a moeda como uma junta frouxa é o da complementaridade intertemporal entre as diversas ordens de bens de capital.  Por exemplo, a ocorrência de um excesso de bens de ordens elevadas (bens de capital), que será removido no futuro através do surgimento de bens de ordens inferiores (por exemplo, cimento para ser usado na indústria de construção civil), não é visto imediatamente como um excesso.  Essa percepção depende das projeções empresariais sobre a demanda futura e essa previsão só poderá ser considerada errada quando ocorrer um excesso nos estágios subsequentes de produção.  Na literatura austríaca, esse excesso se revelará na forma de escassez relativa de bens de capital necessários para completar o processo de produção.  Esse cenário particular enfatiza a noção de complementaridade intertemporal entre as diferentes ordens de bens de capital e o ponto significante é que os excessos de oferta e de demanda, uma vez revelados,  não podem ser remediados de uma forma simples: os ajustamentos envolvem reestruturações fundamentais nos processos econômicos de produção. 

 

                     O caráter de frouxidão (looseness) da junta monetária é responsável por um montante de ausência de coordenação intertemporal não percebida, durante algum tempo, o que gera sobreinvestimentos em alguns bens de capital e subinvestimentos em outros.  Assim, não existe surpresa no fato de que o processo de mercado, ao corrigir essas faltas de coordenação, não o faça instantaneamente e sem dor.   A frouxidão da junta monetária, que provoca a quebra da coordenação, prescreve um remédio doloroso e de efeito lento.  A teoria hayekiana é consistente com a visão da chamada escola da public choice das decisões políticas, com a noção de ciclos econômicos políticos e com a análise friedmaniana da curva de Phillips de curto prazo e de longo prazo.

 

 

 

VII.  Alguns avanços na mainstream economics

 

 

         Sugere-nos o velho bom senso que as fronteiras da macroeconomia devem estar entre as duas posições extremas, em algum ponto a ser descoberto entre o curto e o longo prazo.  Este é o horizonte temporal relevante, porque é nele que os problemas econômicos efetivamente se manifestam.  Há algum tempo têm sido feitas algumas tentativas na literatura no sentido de tentar capturar esse horizonte de tempo relevante; e elas diferem na medida em que partem de pontos de partida diametralmente diferentes.  Em particular, vejamos os pontos de vista de David Laidler e de Paul Davidson - respectivamente, um monetarista e um keynesiano —, o que nos facilitará a comparar a mainstream com a teoria austríaca.

 

         Laidler percebeu corretamente a importância do tempo e da moeda, mas, como parte da tradição de longo prazo da teoria quantitativa da moeda, sua análise revela a necessidade de "encurtar o longo prazo" e o meio que utiliza para isso é o mercado de informações.  A hipótese de que existem custos para obter informações representa sua tentativa de capturar, pelo menos parcialmente, a dimensão temporal e a interação entre o mercado de informações e o mercado monetário, traduzindo o que ele chama de "a nova microeconomia".  O problema com esse tipo de approach é que a incerteza, na visão dos economistas austríacos, é uma proxy fraca para a variável tempo.

 

                     O trabalho de Davidson representa a contrapartida keynesiana da análise de Laidler; este precisava encurtar o longo prazo, enquanto Davidson, trabalhando na tradição keynesiana, sentiu a necessidade de criar uma junta onde ela não existia, ou seja, percebeu que precisava alongar o curto prazo.  E o instrumento de que se utilizou foi o dos contratos de salários nominais (money-wage contracts ).  Assim, enquanto Laidler buscou capturar o tempo através do mercado de informações, Davidson o fez por meio do mercado de trabalho a termo.  Sua análise padece do mesmo problema apresentado pela de Laidler, porque o mercado a termo de mão de obra, tal como o mercado de informações de Laidler, são apenas duas dentre as inúmeras maneiras de realização das trocas intertemporais. 

 

         Embora essas tentativas representem esforços de incorporar corretamente as relações entre tempo e moeda, o que as torna incompletas é que ambas carecem de uma teoria do capital, que nada mais é do que uma teoria geral a respeito da natureza das relações intertemporais que caracterizam a economia e do modo como essas relações podem ser modificadas por mudanças paramétricas ou de política econômica.  Portanto, a teoria do money-wage contract é o meio que Davidson usa para se aventurar no conceito de tempo relevante  (para encontrar Laidler, que se aventurou partindo do outro extremo), mas sem ter de lidar com os problemas básicos com que a teoria do capital se defronta.

 

 

 

VIII.  Conclusões

 

 

                     Reconhecer que os conceitos de tempo e de moeda devem ser centrais na teoria macroeconômica é definir o domínio da macroeconomia como sendo a interação entre o "mercado de tempo" e o "mercado de moeda".   Esta concepção da macroeconomia permite comparar as visões da mainstream e mostrar como elas lidam indiretamente com o elemento tempo, por não incorporarem uma teoria do capital.  A inadequação dessas teorias sugere que o remédio apropriado parece ser o dos austríacos e que as duas visões da mainstream poderiam estar mais próximas a partir de uma reincorporação da teoria do capital na macroeconomia, por representar o tratamento correto do elemento tempo e, assim, contribuir para um entendimento mais adequado do mundo real, que, no final das contas, deve ser a preocupação principal dos economistas.

 

                     A economia real pode ser adequadamente definida, de acordo com a visão austríaca, como ação humana ao longo do tempo sob condições de incerteza genuína (ou seja, não probabilística); o tempo é um fluxo permanente de novas experiências, ou seja, de ações sucessivas sempre praticadas com o objetivo de aumentar a satisfação individual, fluxo esse que não está no tempo (isto é, no eixo newtoniano do tempo), mas que é o próprio tempo.  Portanto, o tempo deve ser necessariamente dinâmico, porque permite associar a memória, vale dizer, o conhecimento adquirido nos mercados e que é permanentemente rarefeito, com a percepção que cada indivíduo forma da realidade e suas consequentes ações e a moeda é o meio de troca, aquele que é usado em todas as transações econômicas. 

 

                     Quando a moeda "entra" na economia ela não o faz por igual, por exemplo, uma cédula de 50 reais por cada metro quadrado do país: ela "entra" em determinados setores da estrutura de produção e a partir daí vai se espalhando, o que, por si só, é um argumento contrário ao princípio da neutralidade da moeda, segundo o qual variações nesta não produziriam efeitos sobre o setor real da economia, mas apenas sobre os preços.  Na verdade, justamente porque a moeda, ao "entrar" na economia, o faz, para usamos a imagem de Hayek, como um fluxo de mel sendo despejado em um pires, os preços relativos — formados ao longo da estrutura de capital (ou, em linguagem moderna, na cadeia produtiva) —, necessariamente precisam se alterar, o que provoca efeitos sobre o setor real da economia.  Somente após todas as mudanças nos preços relativos acontecerem é que o montículo central de "mel" termina de se assentar, tornando a superfície plana, tal como a teoria monetarista sugere. 

 

                     Se a "nova" moeda cai primeiro em mãos de pessoas muito pobres, provavelmente elas irão utilizá-la para comprar alimentos e roupas, o que aumentará a demanda por esses produtos e tenderá a elevar os seus preços em relação, digamos, aos preços de automóveis.  Os vendedores de alimentos e roupas, por sua vez, ao receberem dinheiro pelas suas maiores vendas, comprarão mais de seus fornecedores e assim por diante, até que a moeda "nova" se espalhe por toda a estrutura de produção.  Esta é uma das vantagens que a teoria do capital proporciona, integrada com a teoria monetária.               

 

                     Diante de todas essas considerações, parece claro que os economistas deveriam ter acesso, desde os cursos de graduação, aos ensinamentos da Escola Austríaca, que oferece insights muito interessantes para uma compreensão adequada dos fenômenos econômicos do mundo real.  Na pior das hipóteses, tais ensinamentos servem, sem dúvida, para auxiliar a comparar as metodologias utilizadas pelos teóricos das diversas escolas econômicas.

 