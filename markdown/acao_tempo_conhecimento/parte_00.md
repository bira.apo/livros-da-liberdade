# Ação, Tempo e Conhecimento: A Escola Austríaca de Economia

Autor: Ubiratan Jorge Iorio  é economista, Diretor Acadêmico do IMB e Professor Associado de Universidade do Estado do Rio de Janeiro (UERJ).  Visite seu website.

## Prefácio do autor

Quando aceitei o desafio proposto por meu amigo Helio Beltrão, presidente do Instituto Mises do Brasil, para publicar um segundo livro sobre a Escola Austríaca de Economia, três pensamentos assomaram de relance à superfície do tonel onde estão arquivadas minhas lembranças.
            O primeiro - que procurei afastar imediatamente - é que estou ficando velho...  De fato, já se vão dezesseis anos desde a publicação da primeira edição de Economia e Liberdade: a Escola Austríaca e a Economia Brasileira, pelo Instituto Liberal de São Paulo.  Mas, como tudo tem um lado bom, (omnia in bonum, como escreveu São Paulo) pensei imediatamente que, decorrido esse tempo e mais uns cinco ou seis anos desde que comecei a ler os austríacos, minha experiência e domínio sobre o assunto devem ter aumentado consideravelmente.  Felizmente, o tempo também joga a favor, especialmente para quem é acadêmico... 

            A segunda lembrança foi de quando — já sendo um economista com alguns anos de doutorado e com razoável experiência de ensino e de mercado — Og Leme e Donald Stewart Jr. me apresentaram à Escola Austríaca de Economia, com o reforço de peso, meses depois, de Roberto Campos.  À memória dos três credito o fato de terem instigado minha curiosidade, o que me transformou, já nas primeiras páginas de Ação Humana, de Mises, de um monetarista ex-aluno de Alan Meltzer e novo clássico devorador dos artigos de Robert Lucas e Thomas Sargent, em um austríaco.  

            E a terceira foi efeito da segunda: muitos colegas passaram a me olhar intrigados, como se desejassem me perguntar algo como "o que aconteceu com você, Iorio"? Confesso que algumas vezes senti que me consideravam como algo semelhante a um OVNI, um objeto voador não identificado.  E desconfio até hoje que alguns devam ter pensado com seus botões em aconselhar-me a procurar um psiquiatra.  A vida de um economista austríaco, no mundo inteiro, definitivamente, não é fácil, principalmente nos meios ditos acadêmicos...  Lembrei-me de que essas atitudes me incomodavam na época, mas que, há alguns anos, aprendi a aceitá-las com bom humor, a melhor atitude para quem sabe que eles, os críticos, não sabem bem do que estão falando, simplesmente porque não leram os austríacos... 

            Perguntam-me frequentemente o que vem a ser a Escola Austríaca de Economia.  E essa questão não vem apenas de meus alunos dos cursos austríacos que venho ministrando há bastante tempo na Faculdade de Ciências Econômicas da Universidade do Estado do Rio de Janeiro (UERJ), nem de outros cursos de pós-graduação em que costumo dedicar algumas aulas para tratar do tema.  São, também, jornalistas, administradores de empresas e muitos colegas economistas (professores e não professores), bem como profissionais de outras áreas das ciências sociais e, até, de outros ramos das ciências, como engenheiros e físicos. 

            O que pude concluir desde muito cedo - ilação que só tem se robustecido com o passar dos anos — é que existe uma enorme ignorância a respeito de Menger, Böhm-Bawerk, Mises, Hayek, Kirzner e outros brilhantes economistas da tradição austríaca.  O "conhecimento" máximo que a maioria costuma revelar é que os membros dessa escola "defendem o livre mercado", ou que "são contra o estado[1]", ou, ainda, "que querem privatizar tudo o que encontrarem pela frente".  Quando se trata de economistas, a única informação que demonstram ter sobre o assunto, adquirida em uma ou duas aulas da disciplina de História do Pensamento Econômico, é que o fundador da Escola Austríaca, Carl Menger, foi um dos descobridores da teoria da utilidade marginal (os outros foram Walras e Jevons).  Tenho percebido também com os anos que muitos economistas a criticam sem sequer conhecê-la, uma atitude, digamos, "pouco científica"...  

            Até os anos 30 do século XX, quando aconteceu o famoso debate entre Hayek e Keynes sobre as causas da Grande Depressão, os economistas austríacos eram estudados nos currículos dos cursos de economia em pé de igualdade com os neoclássicos e os marxistas.  Mas, como a versão de Keynes acabou prevalecendo, a Escola Austríaca foi jogada injustamente no deserto do ostracismo.  Apenas em 1974, quando o mundo experimentou pela primeira vez a estagflação (que a teoria austríaca dos ciclos previa desde 1912, quando Mises publicou a Teoria da Moeda e do Crédito), é que se voltou a dedicar alguma atenção a ela, com a concessão a Hayek do Nobel — que, mesmo assim, teve que dividir com um economista socialista sueco, Gunnar Myrdal.  Na década seguinte, o pensamento da Escola Austríaca influenciou a política econômica de Margaret Thatcher e também, indiretamente, a de Ronald Reagan, mas foi só isso...  A vida dos economistas austríacos continuava difícil...  

            E segue assim até os dias atuais, mesmo depois do estouro da bolha imobiliária nos Estados Unidos em 2008, cujas causas foram exatamente as que os austríacos sempre identificaram - e os fatos atestam isto sobejamente — como os germes deflagradores dos ciclos econômicos.  Mas a crise mundial de 2008 e 2009 nos ensinou que é tempo de virarmos o jogo e colocarmos a Escola Austríaca no lugar de destaque que merece. 

            Três grandes motivos - que fui descobrindo, entre encantado e perplexo - forjaram a convicção de que a Escola Austríaca precisa ser novamente estudada sistematicamente pelos economistas, desde a sua formação nas universidades.  

            O primeiro é que os fatos atestam sobejamente que ela tem muito a dizer e a ensinar, o que me impõe a obrigação, como economista e professor, de estimular o maior número possível de futuros profissionais do ramo — e também de outras áreas — a conhecê-la.  O segundo é que ela funciona mesmo -"it works!" -, como me afirmou o economista austríaco Mark Thornton por ocasião do I Seminário de Economia Austríaca promovido pelo Instituto Mises do Brasil, em Porto Alegre, em abril de 2010, porque explica corretamente a ação humana no mundo real, ao preocupar-se, mesmo em suas formulações teóricas, com a economia do dia a dia, (economy), e não apenas com os aspectos teóricos (economics).  E o terceiro é o seu caráter humanista, porque analisa a economia não como um compartimento estanque e sem comunicação com outros setores, mas de uma forma integrada com as demais atividades sociais de natureza política, jurídica, psicológica, histórica, antropológica, ética e cultural, ao amparo da filosofia e no bojo de uma teoria geral da ação humana.  A Escola Austríaca rejeita o homo oeconomicus a que se restringe a quase totalidade dos livros-textos da teoria econômica convencional, porque considera o homem, a pessoa humana, em sua plenitude - e não apenas suas ações econômicas. 

            Somando tudo isso e um algo mais, aceitei o desafio de publicar este segundo livro sobre o tema.  O algo mais é o estímulo triplo representado (a) pelo convite do Helio; (b) pelo incentivo de muitos alunos, especialmente os da Faculdade de Ciências Econômicas da UERJ, que mostraram, para minha surpresa e alegria, uma vontade grande de conhecê-lo e estudá-lo, seja nas disciplinas eletivas em que o abordo, seja nas dezenas de monografias de conclusão de curso com temas austríacos, apresentadas ao longo dos anos; (c) e, por fim, por mensagens que recebo frequentemente de estudantes de economia de todo o Brasil, queixando-se da orientação keynesiana e marxista que eiva os currículos de nossa ciência e pedindo que os oriente de alguma forma.   

            Sobre o livro, cabe mencionar que o título - Ação, Tempo e Conhecimento - representa o coração da Escola Austríaca de Economia.  Enfeixa uma introdução e dez capítulos.  A introdução e seis desses capítulos foram escritos recentemente (de outubro de 2009 até agosto de 2010), desde que encarei o desafio do Helio; os quatro restantes foram escritos entre 2003 e 2009, na forma de artigos para meu site pessoal (www.ubirataniorio.org) e de papers apresentados em conferências e palestras, a que dei nova forma e roupagem, para efeitos de padronização.  Mesmo assim, é inevitável que alguns conceitos sejam eventualmente abordados em mais de um capítulo, embora a revisão final tenha buscado evitar o excesso de repetições.  Mas, por outro lado, um dos benefícios da repetição é ajudar a fixar melhor os temas relevantes.  No intuito de tornar mais leve a leitura de um assunto normalmente pesado, achei por bem não dar tratamento acadêmico formal ao livro e, por essa razão, não coloquei notas de rodapés e registrei apenas as referências bibliográficas estritamente necessárias, mas sempre no corpo do próprio texto. 

             Expresso minha sincera gratidão ao Helio Beltrão, que tem feito um trabalho extraordinário de divulgação da Escola Austríaca em nosso país à frente do Instituto Mises do Brasil.  Agradeço também aos meus alunos da UERJ, pela motivação que me passam ano após ano.  Eles me fazem acreditar que, mesmo em um país que maltrata a educação e os que a ela se dedicam, sempre é gratificante ensinar.      

  

Rio de Janeiro, 19 de março de 2011

 

O autor

           

 ____________________________________________________________________________

[1] N.E.: O editor, Instituto Ludwig von Mises Brasil, em todas as suas obras, opta pela a grafia "estado" com letra "e" minúscula, embora a norma culta sugira a grafia "Estado".   Assim como o Instituto Mises Brasil, a revista Veja adota a grafia "estado" desde 2007.  À época, Veja argumentou que "se povo, sociedade, indivíduo, pessoa, liberdade, instituições, democracia, justiça são escritas com minúscula, não há razão para escrever estado com maiúscula.".  Este editor concorda.  A justificativa de que a maiúscula tem o objetivo de diferenciar a acepção em questão da acepção de "condição" ou "situação" não convence.   São raros os vocábulos que somente possuem um único significado, e ainda assim o contexto permite a compreensão e diferenciação dos significados.  Assim como Veja, o editor considera que grafar estado é uma pequena contribuição pra a demolição da noção disfuncional de que o estado é uma entidade que está acima dos indivíduos.

## Introdução - A ESCOLA AUSTRÍACA DE ECONOMIA

I.  Introdução

 

                             

                  A tradição iniciada por Carl Menger com a publicação, em 1871, de Grundsätze der Volkwirthschaftslehree (Princípios de Economia Política), é um vasto, fascinante e formidável campo do conhecimento humano, que transcende a economia para abastecer-se sistematicamente no âmbito mais abrangente das ciências sociais, nutrir-se continuamente com a discussão filosófica e impregnar-se permanentemente da boa cultura humanista.  Não foi por acaso que Hayek afirmou que um economista que só enxerga dentro dos limites estritos da teoria econômica, por mais apurados que sejam seus conhecimentos técnicos, nunca será um economista completo.  Para a tradição austríaca não basta que ele domine o estado das artes em sua ciência: é preciso ir muito mais além, é preciso ser, mais do que qualquer outra coisa, um humanista.  No entanto, mesmo em se tratando de um campo muito abrangente do conhecimento humano, a Escola Austríaca guarda uma simplicidade que chega a impressionar, que se explica pela lógica irrepreensível de suas proposições e postulados.  Como escreveu Mises, "good economics is basic economics"!

 

                  A esse respeito, pode-se afirmar que os grandes economistas austríacos do século XX — especialmente Mises e Hayek — foram de um destemor exemplar, porque, mesmo tendo vivido em uma época em que seus colegas faziam questão de progressivamente se tornarem crescentemente especializados em áreas cada vez mais restritas da economia, não permitiram, em nenhum momento, que o modismo os fizesse abrir mão de serem generalistas, não no sentido mais vulgar que essa palavra vem adquirindo ultimamente, mas no de valorizarem a vasta cultura e o humanismo.  Infelizmente, os economistas, desde a segunda metade do século XIX, com o abandono da tradição humanista, ao mesmo tempo em que dominavam mais conhecimentos técnicos específicos, foram se tornando cada vez menos cultos, e hoje em dia é lamentável verificarmos que raros são aqueles realmente eruditos, no sentido de dominarem conhecimentos que ultrapassem aqueles contidos nos manuais de microeconomia e de macroeconomia.  Muitos, infelizmente, chegam a tratar com desdém as demais ciências sociais, porque, em sua maneira obnubilada de encarar o conhecimento — que lhes foi ensinada desde os cursos de graduação — estas não seriam "científicas".

 

            Mas a afirmativa de que a infinidade de ações que caracterizam a economia do mundo real costuma ser muito diferente e muito mais complexa do que a que se encontra nos livros- textos deverá se tornar mais clara na medida em que o leitor for avançando nesta introdução, que tem como propósito exatamente relatar todos esses motivos, ou, pelo menos, os mais importantes e, naturalmente, ao longo do livro.

 

            Quando se estuda a Escola Austríaca, analisa-se não apenas a economia, mas as relações desta com a epistemologia, a política, o direito, a história, a sociologia, a psicologia, a antropologia e a filosofia política.  Examina-se, de um lado, como a economia é influenciada por todos esses ramos do conhecimento e, de outro, quais as suas implicações sobre eles, ou pelo menos sobre alguns deles.  Aquele típico homo oeconomicus a que todos os estudantes da economia tradicional são apresentados no início das estruturas curriculares dos cursos de ciências econômicas em todo o mundo, simplesmente, não existe.  É fruto da imaginação, é um fantasma, um espectro, sem qualquer relação com a realidade do dia a dia. 

 

            Não significam essas observações que a teoria convencional deva ser descartada, nem que o homo oeconomicus precise ser esconjurado.  Expressam apenas que os aspectos humanistas da economia não podem ser deixados de lado, como se não fossem importantes ou "científicos", ou como se no máximo fossem meras evocações de um passado nostálgico, repleto de certa melancolia do apogeu do império austro-húngaro, época em que Menger a fundou e que Wieser, Böhm-Bawerk, Mises, Hayek e outros conheceram de dentro.  Na verdade, o humanismo em economia é muito anterior a Menger: remonta a São Tomás de Aquino e, depois, aos autores da chamada Escolástica Tardia e prossegue com David Hume e Adam Smith e só foi "descartado" a partir do século XX, com o avanço das ideias positivistas.  Tanto na vida do dia a dia quanto no mundo da ciência, o que importa não é o homo oeconomicus, mas o homo agens. 

 

            O quadro sinóptico seguinte é uma tentativa de apresentar uma visualização da extraordinária complexidade que constitui o cerne da Escola Austríaca de Economia.  Evidentemente, se trata de um ensaio simplificado, apenas para tentar mostrar ao leitor quais são as peças, o papel que cada uma desempenha e como se encaixam no tabuleiro.  A grande tarefa dos economistas é construir modelos teóricos que consigam explicar razoavelmente a realidade da economia, formada pela ação ao longo do tempo de bilhões de pessoas humanas de carne e osso, com todos os desejos, aspirações, motivações, qualidades e defeitos característicos.  Vejamos em seguida um resumo de cada um dos elementos constitutivos do quadro, que serão naturalmente expostos com mais pormenores ao longo do livro.

 

 

 II.  A tríade básica ou núcleo fundamental

 

 

            A Escola Austríaca tem como fundamentos uma tríade concomitante e complementar, formada pelos conceitos de ação humana e de tempo dinâmico e pela hipótese acerca dos limites ao nosso conhecimento.  Esses três elementos formam o seu núcleo fundamental e se transmitem por meio de seus elementos de propagação para os diversos campos do conhecimento humano.    Essa propagação e suas implicações na filosofia política, na epistemologia e na economia serão analisadas nas seções seguintes.  Nesta, tratamos da tríade constitutiva do núcleo fundamental.

 

            Como o título sugere, esses três elementos são por assim dizer a pedra angular do monumental edifício teórico que constitui a Escola Austríaca de Economia.  Por analogia com a biologia, representam os elementos essenciais, ou seja, aqueles necessários para o desenvolvimento e a manutenção do organismo, e são a um só tempo os macronutrientes ou os micronutrientes de todo o sistema.  Deles emanam os elementos de propagação e neles se assentam todos os elementos essenciais às deduções lógicas e às propostas de natureza prática. 

 



 

 

(a) ação

 

 

            Ação, para a Escola Austríaca, significa qualquer ato voluntário, qualquer escolha feita deliberadamente com vistas a se passar de um estado menos satisfatório para outro, considerado mais satisfatório no momento da escolha.  A praxeologia (de práxis)[1] é a ciência geral que se dedica ao estudo da ação humana, considerando todas as suas implicações formais.  Ora, todos os atos econômicos, sem exceção, podem ser reduzidos a escolhas realizadas de acordo com o conceito seminal de ação humana.  E a proposição básica, o primeiro axioma da Praxeologia, é que o incentivo para qualquer ação é a insatisfação, uma vez que ninguém age a não ser que sinta alguma insatisfação e avalie que uma determinada ação venha a melhorar seu estado de satisfação, ou seja, aumentar seu conforto, sensação de alegria ou de realização, diminuindo, portanto, seu desconforto, frustração ou insatisfação.

 

            Este axioma é universal: onde quer que existam pessoas existirá ação assim definida.  Portanto, a ciência econômica construída com base na praxeologia é, por corolário, universal.  Não há teorias econômicas específicas ou particulares para cada país ou região, mas uma teoria econômica epistemologicamente correta, que é a que se monta peça por peça a partir da observação e do estudo sistemático da ação.  Mises denominou o conceito de ação humana de axioma praxeológico número um, no sentido de que a partir dele podem-se deduzir as principais leis comportamentais que regem a economia.

 

(b) concepção dinâmica do tempo

 

 

            O segundo componente da tríade é a concepção dinâmica do tempo, ou tempo subjetivo, ou, ainda, tempo real, em que o tempo deixa de ser uma categoria estática que possa ser descrita por um simples eixo horizontal, para ser definido como um fluxo permanente de novas experiências, que não está no tempo, como na concepção estática ou newtoniana, mas que é o próprio o tempo.  Quando consideramos o tempo dinâmico, estamos implicitamente aceitando o fato de que algo de novo sempre está acontecendo e assumindo suas três características: continuidade dinâmica, heterogeneidade e eficácia causal. 

 

            O tempo dinâmico real é irreversível e sua passagem acarreta uma evolução criativa, ou seja, implica alterações imprevisíveis. 

O conceito de tempo real é fundamental para que se possa entender a natureza da ação humana: agindo, os indivíduos acumulam continuamente novas experiências, o que gera novos conhecimentos, o que, por sua vez, os leva a alterarem frequentemente seus planos e ações. 

 

 

(c) limitação do conhecimento

 

 

            O terceiro elemento da tríade básica da Escola Austríaca de Economia é o tratamento epistemológico do fato — indiscutível — de que o conhecimento humano contém sempre componentes de indeterminação e de imprevisibilidade, o que faz com que todas as ações humanas produzam efeitos involuntários e que não podem ser calculados a priori.  Existem, para os austríacos, limites inescapáveis à capacidade da mente humana que a impedem de compreender integralmente a complexidade dos fenômenos sociais e econômicos.  Os sistemas formais possuem certas regras de funcionamento e de conduta que não podem ser previamente determinadas.  É como escreveu José Ortega y Gasset: "o olho não se vê a si mesmo". 

 

            Como não é possível quantificar todo o nosso conhecimento, a Escola Austríaca não analisa os mercados como estados de equilíbrio, mas como processos de descoberta e articulação de conhecimentos que, normalmente, na economia do mundo real, permanecem calados, silenciosos, escondidos, espalhados e desarticulados, à espera da inteligência humana subjetiva exatamente para despertá-los, exibi-los, organizá-los e articulá-los.  Esta terceira hipótese nucléica da Escola Austríaca, para diversos estudiosos de epistemologia, é a mais importante.  No entanto, preferimos considerá-la em pé de igualdade com as duas primeiras, por acreditarmos que assim procedendo fica mais fácil destacar as interações e a interdependência existentes entre as três.

  

 

III.  Os elementos de propagação

 

 

(a) utilidade marginal

 

 

            O primeiro elemento de propagação da Escola Austríaca não é exclusivo dela.  Trata-se do conceito ou doutrina da utilidade marginal que, como se sabe, foi a resposta correta, encontrada isoladamente, no ano de 1871, por três economistas, à denominada questão do valor, que vinha desafiando todos os que se interessaram pela ciência econômica, desde São Tomás de Aquino, ainda no século XIII.  Cerca de seiscentos anos depois da Suma, Carl Menger, Leon Walras e William Stanley Jevons, o primeiro em Viena, o segundo em Lausanne e o terceiro em Londres, perceberam que o valor de um bem ou serviço é determinado por sua utilidade marginal em cada momento do tempo, isto é, que o valor depende de uma combinação simultânea da utilidade com a escassez. 

 

            Embora o conceito tenha sido introduzido na teoria econômica pelos três, cada um deles o trabalhou individualmente segundo sua própria convicção: Menger adotou uma postura subjetivista, enquanto Walras (o precursor da chamada escola de equilíbrio geral) e Marshall (o pai da escola de equilíbrio parcial) dispensaram-lhe tratamento matemático, já que o conceito de unidades marginais ou adicionais de bens e serviços encaixava-se perfeitamente no aparato do cálculo diferencial.  Para os austríacos, o princípio da utilidade marginal, a ação, o tempo dinâmico e o subjetivismo são inseparáveis.

 

 

 

(b) subjetivismo

 

 

            O subjetivismo da Escola Austríaca não se limita à teoria subjetiva do valor ou à percepção de que as teorias que lidam com o campo humano seriam pessoais e, portanto, não sujeitas a testes, mas refere-se a uma pressuposição básica: a de que o conteúdo da mente humana — e, portanto, os processos de tomadas de decisão que caracterizam nossas escolhas ou ações — não são determinados rigidamente por eventos externos. 

 

            Assim, o subjetivismo enfatiza a criatividade e a autonomia das escolhas individuais e, por conta disso, subordina-se ao individualismo metodológico, à concepção de que os resultados do mercado podem ser explicados em termos dos atos de escolha individuais.  Para os austríacos a teoria econômica deve considerar prioritariamente o emaranhado de fatores que explicam as escolhas e não limitar-se a simples interações entre variáveis objetivas.

 

            O subjetivismo, então, analisa a ação humana levando em conta que essa ação se dá sempre sob condições de incerteza genuína, não mensurável, e, também, que ela necessariamente acontece ao longo do tempo dinâmico.  Quando um agente escolhe um curso de ação, os resultados de sua escolha vão depender dos cursos de ações executadas e a serem potencialmente executadas por outros indivíduos.  Prevalecendo a autonomia nas decisões individuais, isto quer dizer que o futuro não pode ser conhecido e nem aprendido.

 

           

 

(c) ordens espontâneas

 

 

            Ordens espontâneas são classes intermediárias de fenômenos que são específicos da ciência da ação humana ou Praxeologia.  São, por assim dizer, instituições que se situam entre o instinto e a razão, resultantes da ação humana, mas não da execução de qualquer desígnio humano.  Com efeito, para os pensadores da Grécia antiga, existiam dois tipos de fenômenos, correspondentes aos termos — introduzidos pelos sofistas do século V a. C. — physei, que significa "por natureza" e thesei, que significa "por decisão deliberada".   

 

            Para os austríacos, entretanto, essa dicotomia não é condizente com as ciências sociais.  No dizer de Hayek, em The Counter-Revolution of Science: Studies on the Abuse of Reason (Collier-Macmillan, New York-London, 1964, p. 39): "alguma espécie de ordem aparece como resultado da ação individual, mas sem ser intencionada por qualquer indivíduo".  Exemplos característicos dessas ordens são o sistema monetário, os mercados, as manifestações culturais e a linguagem. 

 

            Como salienta o professor português José Manoel Moreira, da Universidade de Aveiro, em sua tese doutoral apresentada na Universidad Pontifícia Camillas (Madri), publicada em edição revista e abreviada pela Universidade do Porto em 1994, "o contraste é entre uma ordem espontânea ou auto-gerada ou endógena, e uma ordem construída ou exógena ou ordem artificial, ou mesmo uma organização, quando se trata de uma organização social dirigida" (Filosofia e Metodologia da Economia em F. A. Hayek, p. 187).   Continua o Professor  Moreira na mesma página: "Hayek, apesar da conotação autoritária que o próprio termo "ordem" tem, em especial para as pessoas que se negam a admitir uma ordem que não seja deliberadamente criada pelo homem, insiste em manter o termo "ordem", ou melhor, "ordem espontânea" ou "kosmos" para definir o objeto das ciências sociais e que nascem da descoberta da existência de estruturas ordenadas que são o resultado da ação de muitos homens, embora não sejam o resultado do desígnio humano".

 

            A economia do mundo real, desde que os homens descobriram que poderiam obter ganhos com o processo de trocas até os nossos dias é uma grande ordem espontânea, semelhante ao universo, em que há permanentemente forças em expansão e em contração, razão pela qual os austríacos costumam denominar a economia de mercado de cataláctica ou cataláxia.

 

 

IV.  Filosofia política

 

 

            A filosofia política da Escola Austríaca deve ser vista como uma tentativa de compreender e explicar a história e as instituições sociais à luz dos limites naturais ao conhecimento humano.  Como escreveu o filósofo italiano Raimondo Cubeddu, professor em Pisa, "a história e as instituições sociais aparecem frequentemente como produtos das ações humanas individuais, voltadas para a consecução de fins subjetivos" (The Philosophy of the Austrian School, Routledge, London-New York, 1993, p. x do Prefácio).  Portanto, Menger, Mises, Hayek e outros austríacos não foram apenas economistas que mergulharam no mundo da política, ou sonhadores de um mundo melhor de cunho utópico, mas pensadores que elaboraram uma teoria do melhor regime baseada em uma concepção da ação humana e da natureza da sociedade.

 

            Na verdade, o início da chamada era moderna foi marcado pela emancipação da economia da ética e da política, ou seja, pela suposição de que a economia, diferentemente desses dois outros campos do conhecimento, teria o status de "ciência".  Mas também é verdade que, nos dias atuais, as discussões sobre o melhor regime sempre aparecem entremeadas com as questões econômicas.  Porém, quando Mises e Hayek recusaram-se a aceitar a separação da economia da ética e da política, os acadêmicos, embevecidos pelo canto da sereia do positivismo, não pensavam assim.  Quando os dois austríacos publicaram suas críticas ao socialismo, advertindo que sua adoção acabaria gerando o caos ou a tirania, precisamente porque jamais seria possível conciliar planejamento e liberdade, eram como que vozes solitárias clamando no deserto.  Naquela primeira metade do século XX havia uma crença geral de que os perigos apontados por Mises, Hayek e outros com relação ao socialismo, simplesmente, não existiam, que suas críticas tinham tão somente motivações de natureza ideológica e que embutiam uma visão errada dos mecanismos que proporcionam o crescimento das economias.

 

            É espantoso que ainda hoje, depois do fracasso de todas as experiências socialistas, ainda haja predominância nas academias, em todo o mundo, de intelectuais que simpatizam com o socialismo.  Há olhos que se recusam a enxergar.  A filosofia política não é uma forma de ideologia e não pode ser reduzida a um mero conhecimento da história, nem representa uma simples expressão de uma "luta de classes".  Ela é um desafio filosófico, guiado pela razão e pela experiência em busca do melhor regime político.  Em suma, é o que a Escola Austríaca jamais deixou de sustentar.

 

           

 

(a) crítica aos sistemas mistos

 

 

            Com base na antítese entre ordens espontâneas e ordens dirigidas Hayek faz uma distinção importante a cerca das normas do sistema político e institucional.  As primeiras, do tipo nomos, são as que evolvem de maneira não deliberada, em que o sistema jurídico vai paulatinamente, em consonância com os usos, costumes e tradições, estabelecendo as garantias às liberdades individuais básicas para que os casos concretos de violação a essas liberdades sejam solucionados.  Trata-se da common law.  Essas normas devem servir de base ao direito, por serem regras gerais de justa conduta, aplicáveis a todos de modo igual e, ao fim e ao cabo, representam o fundamento para a proposição de que o poder do estado precisa e deve ser limitado.

 

            As regras do tipo thesis, em contrapartida, são deliberadas, planejadas no campo do direito positivo, para a obtenção de objetivos específicos de interesse do poder do estado e dos grupos que ocupam o poder ou que se beneficiam dele.  Exemplos de normas desse segundo tipo são: a obrigatoriedade de se usar cintos de segurança dentro de um automóvel, os orçamentos públicos e as alíquotas de impostos.

 

            Por outro lado, às economias de mercado aplica-se o paradigma cosmos e às economias dirigidas ou planificadas o paradigma taxis.  Direito e economia são, na concepção austríaca, inseparáveis e precisam ser complementares, quando se busca uma ordem social adequada.

 

            Portanto, cosmos-nomos representa uma ordem social em que prevalece a economia de mercado regida por normas de justa conduta que atendem à tradição da common law.  Já uma ordem dirigida no direito e na economia pode ser sintetizada pelo paradigma thesis-taxis, que traduz uma economia dirigida pelo estado, que faz o seu intervencionismo ser suportado por comandos e ordens no campo jurídico.

 

            Para os austríacos, quaisquer formas intermediárias entre uma ordem espontânea e uma ordem dirigida são inviáveis, porque sofrem, por definição, de inconsistências lógicas internas fatais.  Os sistemas mistos — economia de mercado sob comandos legais ou economias dirigidas sob a common law — são inviáveis por sua inconsistência interna, já que uma economia de mercado pressupõe ausência de ordens e controles, enquanto uma economia dirigida não admite as normas características da common law.  Sendo assim, no longo prazo, essas formas mistas de organização jurídica e econômica não conseguem ser sustentadas e tendem a convergir para uma ordem dirigida ou totalitária do tipo thesis-taxis.

 

 

(b) evolução nas ciências sociais

 

 

            Para a Escola Austríaca as sociedades sempre foram ordens espontâneas, desde os primeiros agrupamentos tribais até a moderna civilização de nossos dias.  Sendo assim, as sociedades evolvem não como resultado exclusivo da razão planejada, mas mediante processos de mutações permanentes, de processos evolutivos, que em nada se assemelham ao evolucionismo biológico de Darwin.  O evolucionismo em ciências sociais não é uma extensão do evolucionismo biológico darwiniano.  A verdade é exatamente o oposto: foi Charles Darwin quem aplicou à biologia o conceito de evolução.

           

            A crença de que a teoria da evolução social consiste de "leis de evolução" é completamente equivocada, uma vez que não se pode falar em leis ou sequências definidas, às quais a evolução das instituições sociais deveria estar subordinada.  Logo, não é possível estabelecer modelos de previsão das trajetórias futuras dos fenômenos sociais.  Em outras palavras, a evolução das sociedades não segue um curso pré-determinado; é antes um processo de tentativas e erros e, fundamentalmente, de natureza cultural, ao não resultar nem do instinto e nem da razão.  Como ressaltou Hayek: "a cultura não é um bem natural nem artificial, nem geneticamente transmitida nem racionalmente planejada.  É uma tradição de normas de conduta aprendidas que nunca foi "inventada" e cujas funções os indivíduos que atuam normalmente não compreendem.  Há certamente tanta justificação para falar da sabedoria da cultura como da sabedoria da natureza — ainda que, talvez, por causa dos poderes do governo, os erros da primeira sejam menos facilmente corrigidos" (Law, Legislation and Liberty,The University of Chicago Press, Chicago, 1973, vol. 3, p. 155)

 

 

(c) democracia e divisão de poderes

 

 

            A democracia não pode ser vista como um fim em si, mas como simples meio de assegurar que os governantes eleitos exerçam o seu papel de governar por meio de normas gerais e não se valendo de decretos que expressam seus próprios desígnios.  A democracia deve, por outro lado, conter mecanismos de prevenção contra os abusos da vontade da maioria parlamentar e impor limites à atuação do partido ou coligação que esteja eventualmente no poder.

 

            A finalidade da democracia, para Hayek, é garantir uma verdadeira separação entre os três poderes tradicionais, algo que, apesar das boas intenções do estado constitucional moderno, nunca foi realizado, dado que, tradicionalmente, o poder de legislar e o poder de dirigir o governo — o legislativo e o executivo — sempre se misturaram nas assembléias legislativas e, com o passar do tempo, as instituições democráticas existentes foram se moldando às necessidades dos governos democráticos mais do que à de descobrir sistemas apropriados de regras gerais de justiça para atender ao interesse público.

 

            Assim, as atribuições de legislar têm passado cada vez mais para o âmbito do executivo, restando ao legislativo um papel meramente fiscalizador, servindo a democracia como simples vestimenta ou disfarce, embora indispensável para dar aparato de legalidade a eventuais arbitrariedades do executivo.   Este fato se constitui em uma inversão de valores e, segundo Hayek, é reflexo da transformação da lei (nomos) em legislação (thesis), fazendo com que o compromisso com princípios e normas de conduta gerais fosse substituído por consensos a respeito de medidas particulares. 

 

            A democracia, portanto, não pode ser ilimitada; ela precisa possuir mecanismos de divisão de poderes, inclusive para impedir que governos fiquem reféns de diversos grupos de interesse. 

 

 

(d) contenção do poder

 

 

            Para a tradição austríaca, o grande desafio na organização de uma ordem social que assegure as liberdades individuais e o respeito às normas gerais de conduta é o de limitar efetivamente o poder.  Liberdade, razão e estado são essenciais no processo de conformação da ordem nas sociedades, porque as duas primeiras são inseparáveis das normas de conduta e estas, por sua vez — mesmo que isto nos incomode —,são mais facilmente respeitadas por coerção implícita do que por acordos voluntários.   

 

            Hayek propôs duas assembléias, uma para cuidar das normas de conduta (nomos), e outra a quem caberia, exclusivamente, a legislação (thesis).  A primeira deveria refletir uma representação de acordo com o peso eleitoral dos partidos políticos e a segunda funcionaria de maneira independente destes.  Essas diferentes funções e composições atenderiam concomitantemente ao princípio da separação dos poderes e ao estado de direito, estabeleceriam de modo mais efetivo uma verdadeira primazia da lei sobre os indivíduos e impediriam os detentores do poder executivo de usar o seu poder para atender aos interesses partidários ou pessoais, disfarçando-os com argumentos de "interesse público" ou de "justiça social".  Adicionalmente, esse sistema bicameral impediria que a maioria parlamentar seguisse comportamentos discricionários, de acordo com as conveniências de cada momento político.

 

            A filosofia política de Hayek está longe de ser um modelo abstrato, porque está baseada na evolução da common law inglesa, um sistema jurídico que, mediante a solução para casos concretos, foi ao longo do tempo e desde muitos séculos descobrindo as liberdades individuais.  A evolução das instituições políticas limitativas dos poderes do estado está assentada nessa mesma base.  Na Inglaterra, o surgimento do poder legislativo não foi resultado de nenhuma concepção segundo a qual o direito deve ser deliberadamente "planejado" por uma assembléia colegiada, mas uma medida de caráter mais prático do que teórico para tentar limitar os poderes do rei.  O fato de existirem duas câmaras, a dos lordes e a dos comuns, também se explica pelo mesmo princípio: enquanto aquela se incumbe de defender o direito, mesmo que não sancionado sob a forma de leis escritas, mas já existente na common law, a câmara dos comuns deve preocupar-se com as normas do tipo thesis que, na nomenclatura de Hayek, significa legislação e se diferencia do direito (nomos).   Ao legislativo, pois, não compete elaborar o direito, mesmo porque este já existe na common law. 

 

            Com a forte influência de Rousseau, contudo, esse entendimento foi se degenerando, mas ele é claro ao exprimir que as liberdades individuais devem ser, na maior medida possível, autônomas em relação à vontade arbitrária dos poderes do estado, sejam o executivo ou o legislativo.  Ninguém, nem uma maioria, nem uma minoria, de acordo com a concepção de Hayek, deve possuir qualquer poder decisório sobre o direito, porque este deve basear-se nas liberdades individuais.

 

 

(e) crítica ao construtivismo

 

 

            Muitas das controvérsias existentes no campo científico e, principalmente, no político, derivam das diferenças filosóficas básicas entre duas escolas gerais de pensamento.  Como ressaltou Hayek, embora seja costumeiro referir-se a ambas como racionalismo, deve-se distinguir entre o racionalismo evolutivo (ou, na nomenclatura de Karl Popper, racionalismo crítico) e o racionalismo construtivista (ou ingênuo, no dizer de Popper).

 

            O racionalismo cartesiano, ao ser transplantado para as ciências sociais, gerou a idéia de que a mente e a razão humanas seriam capazes, por si só, de permitir ao homem construir de novo a sociedade.  Essa pretensão racionalista, que Hayek denominou de construtivismo ou de engenharia social, teve suas origens em Platão, fortaleceu-se com Descartes e encontrou seguimento em Hegel e Marx.  Confrontado, com o racionalismo evolutivo característico do pensamento liberal, o racionalismo construtivista — fonte das utopias, do socialismo, do nazismo e do totalitarismo em geral — desponta como ingênuo em suas crenças, extremamente arrogante em sua gnosiologia e perigoso em suas experimentações práticas nas sociedades modernas, como a história do século XX atesta.  Para compreender esse fenômeno, recomendamos a leitura da obra do filósofo alemão Eric Voegelin, especialmente Hitler e os Alemães. 

 

            A posição da Escola Austríaca — o racionalismo crítico ou evolutivo — baseia-se em uma visão de mundo extremamente mais realista em sua observação dos fatos, humilde em relação às limitações dos poderes da mente humana e cética no que diz respeito aos experimentos de engenharia social, um resultado natural da utopia racionalista cartesiana.

 

            Em resumo, há limites claros ao conhecimento e esses limites impedem que as experiências construtivistas obtenham êxito.  Isto não significa, evidentemente, que o corpo teórico da Escola Austríaca não seja constituído por um conjunto de proposições racionais, apenas que o tipo de racionalismo que utiliza é evolutivo ou crítico.

 

 

 

V.  Epistemologia

 

 

            A epistemologia ou estudo do conhecimento científico costuma dividir as ciências em factuais, quando se baseiam na observação e na experimentação; têm objetos reais de estudo e juízos geralmente sintéticos, isto é, em que o predicado não se verifica a partir da análise do sujeito (por exemplo, "o torcedor do Fluminense", pois não se pode, a partir da análise do conceito de "torcedor", inferir que ele tenha amor ao "Tricolor"); e formais, quando têm como objeto entes de razão; utilizam o método axiomático-dedutivo (que consiste em inferir uma série de teoremas a partir de alguns axiomas ou proposições não demonstrativas); e elaboram juízos analíticos de valor (que são aqueles em que o predicado se infere a partir da análise do sujeito, como, por exemplo, o predicado da oração "Deus é infinitamente sábio" se infere a partir da análise do conceito "Deus").

 

            A economia, de acordo com a Escola Austríaca, é uma ciência factual-dedutiva, uma vez que, embora não estude entes de razão como fazem as ciências formais e também não empregue o método experimental, como fazem as ciências factuais, é, rigorosamente, uma ciência dedutiva, na medida em que suas leis não são derivadas do que foi observado em diversos casos, mas se inferem a partir de premissas gerais.  A análise austríaca parte do exame das implicações formais da ação humana, considerada como a característica essencial de tudo o que é "econômico".  Sobre essa base, que é estabelecida por axiomas, ela prossegue, deduzindo suas implicações lógicas, até construir os teoremas correspondentes, que são as leis econômicas.

 

            Uma das características da Escola Austríaca é que ela vê as leis econômicas como relações indicativas de causa e efeito, isto é, apenas como indicativas de tendências, sem exatidão matemática.  Isto decorre do fato de que não existem, em economia, constantes, como existem na física.  Sendo assim, as leis da física, não podendo ser deduzidas do nada, devem ser induzidas, mediante um processo de experimentos prévios, ao passo que, em economia, as medições realizadas não são mais que simples dados da história econômica, sem significado maior para a teoria econômica pura.  Por essas razões é que os economistas da Escola Austríaca sempre sustentaram que o método experimental, utilizado corretamente por ciências como a física e a química, não pode ser utilizado pela economia, cuja metodologia deve necessariamente basear-se em procedimentos dedutivos.

 

 

 

(a) individualismo metodológico

 

 

            O individualismo metodológico da Escola Austríaca remonta a Menger — que elaborou um tratado sobre a essência e os métodos das ciências sociais — e está associado ao seu conteúdo subjetivista que, ao lidar com o espírito individual e com as escolhas individuais, conduz naturalmente ao estudo do indivíduo.  Assim, os resultados dos mercados devem ser explicados em termos dos atos individuais de escolha praticados nesses mercados.

 

            A economia deve tratar, antes de qualquer outra coisa, com pensamentos e avaliações individuais que antecedem e servem de base para as escolhas e não de inter-relações entre magnitudes objetivas.  Esta compreensão do individualismo metodológico já embute, então, críticas aos excessos de quantificação e aos insights holistas (como a macroeconomia, por exemplo) e coletivistas.   

 

            É importante frisar que individualismo metodológico não é sinônimo de egoísmo, já que não existe nenhuma dicotomia entre o individual e o social (este último sendo "altruísta") e que, além disso, não tem por objetivo reduzir as sociedades a meros somatórios de indivíduos. 

 

            O objeto da economia, assim como o das chamadas ciências sociais — deve ser prioritariamente o indivíduo, a pessoa humana com toda a dignidade que tal condição lhe outorga e não um ente abstrato — a "sociedade" — que, embora possa ser considerada como o conjunto de todos os indivíduos que a compõem, não pensa, não fala, não vai às compras, não poupa e não investe.  Quando muito, essas ações são feitas em seu nome, mas por pequenos grupos formados por indivíduos que ocupam de alguma forma o poder.  A "sociedade" é, por assim dizer, uma abstração real, no sentido de que ela existe, mas não tem vida própria, porque não é mais do que um amontoado dos desejos, aspirações, escolhas, sucessos e frustrações, muitas vezes conflitantes, de milhões de indivíduos.

 

            Expressões comumente utilizadas, como "a sociedade civil organizada deve decidir sobre este ou aquele assunto", de inspiração gramsciana, não têm o menor cabimento dentro do contexto do individualismo metodológico.

(b) modelos vs. fatos nas ciências sociais

 

 

            Em Scientism and the Study of Society, Hayek pergunta o que são os fatos nas ciências sociais, frisando que estas não podem ser manejadas como objetos físicos tal como as ciências naturais o fazem, mas sim com conceitos que devem surgir da vontade das pessoas quando estão agindo.  Isto significa, por exemplo, que para a economia não interessa se uma determinada moeda é de cobre ou de ouro, nem se uma cédula emitida pelo Banco Central é verde ou azul, mas sim que a moeda é um meio de troca, ou seja, o que importa é como ela é utilizada nas ações humanas.  A moeda, como conceito geral, não pode ser explicada sem que se refira a interações entre indivíduos.  Trata-se, como vemos, do individualismo metodológico. 

 

            O filósofo argentino Gabriel Zanotti, no brilhante artigo Hayek y la Filosofía Cristiana (www.cepchile.cl/dms/archivo_811_1314/rev50_zanotti.pdf) demonstra que a perspectiva gnosiológica de Hayek — ao defender quais são os fatos das ciências sociais e, ao mesmo tempo, o individualismo metodológico —  é nominalista e neokantiana, ou seja, relaciona o individualismo metodológico com o individualismo ontológico, para o qual somente existem indivíduos e as essências universalizadas em nossa mente não possuem fundamento real.  Para Zanotti, "os conceitos gerais segundo os quais estudamos as interações individuais não se referem a uma essência realmente existente, mas a modelos gerais mentalmente formulados antes de qualquer observação, a que Hayek, por sua vez, relaciona com a limitação do conhecimento e com a ordem espontânea" (p. 60).

 

 

(c) características das ciências sociais

 

 

            Como mencionamos anteriormente, uma das características mais fortes da epistemologia da Escola Austríaca é a idéia de que as ciências sociais precisam ser conduzidas com objetivos e métodos distintos daqueles utilizados pelas ciências naturais, porquanto não devem utilizar o método indutivo daquelas ciências, mas sim modelos gerais de análise das complexas interações sociais, que possam servir de base para a dedução de conclusões que têm, entretanto, pouca utilidade para prever com precisão os resultados precisos de situações particulares.  Esses resultados não podem ser verificados por meio de experimentos controlados, mas podem ser negados (falsificacionismo) pela observação dos fatos.

 

            Portanto, os austríacos rejeitam veementemente o método positivista nas ciências sociais e, portanto, na economia.  Essas ciências não têm por que copiar os métodos das ciências naturais, nem os seus cientistas precisam sentir-se "inferiorizados" por isso, já que a questão central — que muitos economistas parecem ignorar — é que os seus métodos de estudo, suas características e seus objetivos são completamente diferentes daqueles das ciências naturais, tanto sob o ponto de vista ético quanto do antropológico.

 

 

 

(d) previsão em ciências sociais

 

 

            Em Scientism and the Study of Society, uma publicação de 1942, Hayek já antecipava seu ceticismo quanto à capacidade de previsão de "modelos de informação incompleta".  O contexto geral das ciências sociais implicaria modelos de previsão sobre cada ordem espontânea a ser analisada.  A limitação do conhecimento, no entanto, não é apenas um fenômeno existente do lado dos atores ou agentes, mas também no dos cientistas sociais encarregados da elaboração desses modelos. 

 

            Ora, como esperar, então, que alguém que não tem conhecimento de todos os fatores relevantes em fenômenos tão complexos como os estudados pelas ciências sociais possa ter capacidade para prever a trajetória futura desses fenômenos? Trata-se, como observa Zanotti, do "indeterminismo intrínseco à conduta humana", como diria Popper, ou, simplesmente, do "livre arbítrio", como escreveria São Tomás de Aquino.

 

            A capacidade de previsão das ciências sociais é restrita a previsões gerais e jamais a previsões particulares e específicas.  E a única possibilidade de testes empíricos é a pura observação de acontecimentos que, segundo o resultado geral da previsão, seriam impossíveis.

 

            Por isso, quando ainda não conhecia a Escola Austríaca e jornalistas ou alunos me perguntavam, por exemplo, qual seria a taxa de inflação em determinado ano, eu — treinado na macroeconomia e na econometria — sempre tinha um número "pronto" revelado "cientificamente" por algum modelo.  Mas, desde que li Ação Humana e decidi estudar os economistas austríacos, minha resposta a esse tipo de pergunta passou a ser: a tendência da inflação é de alta (ou de queda, se for o caso), mas nem eu e nem ninguém tem capacidade para dizer de quanto será a alta (ou a queda)...

 

 

 

 

VI.  Economia

 

 

 

            A economia da Escola Austríaca, assim como a epistemologia e a filosofia política, também deriva do que denominamos de tríade básica — ação, tempo e conhecimento — e se propaga por meio dos conceitos de utilidade marginal, subjetivismo e ordens espontâneas, que são os seus elementos de propagação.

           

            Com base no núcleo seminal e nesses elementos propagadores, os economistas austríacos, desde os primórdios com Menger, erigiram uma obra extraordinariamente rica sob o ponto de vista científico, mas que funciona perfeitamente — evidentemente, naquilo que se pode chamar de "perfeição" em uma ciência social — quando tenta explicar o mundo real.

 

            Vejamos resumidamente cada um dos seis campos da teoria econômica que consideramos essenciais para a compreensão do pensamento austríaco.

 

 

(a) processo de mercado

 

 

            A Escola Austríaca não estuda, ao contrário da mainstream economics, mercados em estado de equilíbrio.  Nem tampouco utiliza a famosa classificação dos mercados segundo as suas "formas" (concorrência perfeita, oligopólio, concorrência monopolista e monopólio).  Ela trabalha com a hipótese de que os mercados são processos que tendem ao equilíbrio (porque os agentes são racionais e aprendem com os erros), mas que, em cada momento distinto do tempo dinâmico, não estão em suas "posições" de equilíbrio.

 

            Para entender isto, basta mencionarmos os principais elementos da teoria.  Em primeiro lugar, os mercados são movimentados pela ação humana de seus participantes, tanto no lado da demanda quanto no da oferta.  Em segundo lugar, a ação humana se dá ao longo do tempo dinâmico, em que cada instante é uma oportunidade de aprendizado.  Terceiro, as transações nos mercados se realizam sob condições de limitação e de dispersão do conhecimento.  Quarto, os mercados são ordens espontâneas, sujeitando-se, portanto, a permanentes mutações.  Quinto, a ação humana é subjetiva. 

 

            Como esperar, então, que o mundo real possa apresentar mercados em "equilíbrio"? Este é um dos principais pontos da teoria austríaca.  Os mercados são processos de erros e tentativas, um permanente procedimento de descobertas de novas oportunidades, com uma dinâmica que não dá espaço para o equilíbrio.

 

 

(b) função empresarial

 

 

            A função empresarial é a capacidade individual subjetiva de perceber as possibilidades de ganhos existentes nos mercados.  Portanto, ela nada mais é do que uma categoria de ação.  Sendo assim, a ação humana pode ser considerada como um fenômeno empresarial, mais especificamente aquela que realça as capacidades perceptiva, criativa e de coordenação do agente. 

 

            Como em qualquer ação humana, a ação empresarial acontece em ambiente de incerteza genuína, dadas as limitações de nosso conhecimento.  Requer, por sua vez, criatividade e desprendimento, já que o futuro é incerto e uma ação empreendedora tanto pode dar bons resultados como maus resultados.  A ação empresarial é um conjunto de escolhas ao longo do tempo em ambiente de incerteza e, como tal, implica em um conjunto de outras ações alternativas a que se deve forçosamente renunciar e o custo é o valor subjetivo dessas ações a que se renuncia.

 

            Como os meios sempre são escassos face aos fins, os agentes buscam primeiro os fins aos quais dão maior valor e apenas depois os demais, relativamente menos importantes.  Cada ação é motivada pela crença subjetiva de que os fins escolhidos possuem um valor maior do que o valor dos custos da escolha de uma ação e a diferença entre ambos é o lucro, o elemento que explica a ação. 

 

            Para a Escola Austríaca toda ação embute um componente empresarial puro e criativo em sua essência, que não requer qualquer custo e que é exatamente o que permite aproximar o conceito de ação do conceito de função empresarial. 

 

 

(c) debate sobre o cálculo econômico

 

 

            Mises, ainda nos anos 20 do século passado, percebeu claramente que o sistema socialista impossibilita o cálculo econômico.  Seu argumento era simples: o cálculo econômico requer que os planejadores conheçam os preços; estes, por sua vez, para que possam ser considerados como preços de fato (e não pseudo-preços) pressupõem a existência do processo de mercado, em que as ações de demandantes e ofertantes possam fluir normalmente; e os mercados, para que possam existir, requerem a propriedade privada.  Ora, o socialismo não contempla a propriedade privada; portanto, não faz sentido falar em mercados em num sistema socialista; se não há mercados efetivos, não pode haver preços e, não havendo preços, o cálculo econômico torna-se impossível.  Por essa razão, Mises afirmava categoricamente, em seu debate com os economistas socialistas, que o sistema que defendiam guiava-se às cegas e estava, portanto, fadado ao fracasso, pela desorganização social e econômica que embute.  A história comprovou — e ainda está comprovando — que Mises estava certo.

 

            Os órgãos centrais nesses sistemas são formados por pessoas, e não é razoável admitir que por melhores e mais "puras" sejam suas intenções, possuam o dom da onisciência, que lhes permita conhecer e interpretar os conjuntos dispersos de informações individuais, que estão se alterando e renovando ininterruptamente ao longo do tempo. 

 

            Os planejadores nem conseguem saber qual o seu o grau de ignorância sobre as informações necessárias para promover o cálculo correto e a consequente coordenação.  E quanto maior o grau de coerção imposto, menores são as possibilidades de realização dos planos, porque a maior repressão tende a aumentar a ausência de coordenação, provocando distorções nos mercados que são progressivamente crescentes com o tempo.

 

 

(d) teoria monetária

 

            Os pontos principais da Escola Austríaca a respeito da teoria monetária podem ser resumidos em cino: o primeiro é que os efeitos das variações do estoque de moeda afetam desigualmente os preços relativos, a estrutura de capital, os padrões de produção da economia e alteram os níveis de emprego dos fatores de produção.  Já em 1912, em sua obra monumental Teoria da Moeda e do Crédito, Mises chamava a atenção para o fato de que aumentos na oferta de moeda não produzem benefícios para a sociedade, porque eles não possuem capacidade de alterar os serviços de troca proporcionados pela moeda, apenas reduzem o poder de compra de cada unidade monetária. 

 

            O segundo é que os ciclos econômicos são fenômenos que, embora se manifestem no chamado setor real da economia, têm causas exclusivamente monetárias. 

 

            O terceiro é que a moeda, como qualquer outro bem, tem o seu valor decretado pelo princípio da utilidade marginal, como demonstrou Mises naquela obra, ao resolver o então denominado problema da circularidade austríaco, com o seu famoso teorema da regressão, como veremos no capítulo dedicado à teoria monetária da Escola Austríaca.

 

            E o quarto é que os austríacos definem a inflação não como um simples "aumento contínuo e generalizados de preços", uma vez que essa, na verdade, é a manifestação da inflação; eles a definem como uma queda permanente no poder de compra da moeda, provocada, em última instância, pela emissão de moeda e pela consequente diminuição de sua utilidade marginal. 

 

            O último ponto é que a moeda, vale dizer, o sistema monetário, é uma ordem espontânea, um fenômeno que passa permanentemente por evoluções que são resultantes da ação humana, mas não de qualquer planejamento.

 

 

(e) teoria do capital

 

 

            A teoria do capital austríaca, sem dúvida, é um elemento que diferencia essa escola de pensamento de todas as demais, pelo simples fato de que estas não possuem algo que se possa denominar de teoria do capital.

 

            Quem mais contribuiu para uma concepção austríaca do capital foi, sem dúvida, Böhm-Bawerk, que seguiu a tradição iniciada por Menger.  Mises, Hayek e outros austríacos trabalharam fortemente para o seu desenvolvimento.

 

            Seu ponto central é o conceito de estrutura de capital ou estrutura de produção, que considera que um bem, desde que começa a ser produzido até ficar acabado na forma de um bem final, passa por várias etapas no processo produtivo.  Esses diversos estágios correspondem à estrutura de capital da economia.  Portanto, o capital não é homogêneo e muto menos constante, como os modelos macroeconômicos o consideram.  Ele é essencialmente heterogêneo e varia com os demais fatores de produção ao longo do tempo.

 

            A heterogeneidade dos bens de capital e o fato de que as economias possuem estruturas de capital levam, entre outras hipóteses (como a do individualoismo metodológico) os economistas austríacos à rejeição da análise macroeconômica.

 

 

(f) teoria dos ciclos econômicos

 

 

            A ABCT (Austrian Business Cycles Theory) foi desenhada por Mises em seu tratado de 1912, posteriormente desenvolvida por Hayek nos anos 30 e depois aperfeiçoada por outros economistas da tradição de Menger, dos quais o mais criativo é o americano Roger Garrison.

 

            É, ao mesmo tempo, uma teoria da moeda, do capital e dos ciclos econômicos.  Mostra como a emissão de moeda produz o efeito de diminuir a taxa de juros e, inicialmente, enganar os agentes — que, acreditando que se trata de maior poupança, embarcam em investimentos de maturação mais longa, alargando, assim, a estrutura de capital da economia.  Posteriormente, quando esses agentes descobrem que na realidade não se tratava de poupança, mas de moeda "fantasiada" de poupança, a taxa de juros sobe e isso leva a um encolhimento da estrutura de produção, fenômeno que produz desemprego (e que ficou conhecido como efeito concertina ou efeito sanfona), que é maior nos setores mais afastados da produção de bens finais, que foram exatamente aqueles setores inicialmente beneficiados pela expansão monetária.

 

            Assim, a inflação — ou seja, aquela quantidade adicional de moeda que entrou na economia sem lastro — acabará provocando o desemprego de fatores de produção.  Como disse Hayek, não há escolha entre comer demais (emitir moeda sem lastro real) e ter indigestão (recessão), porque ambas são inseparáveis, a primeira acarretando a segunda.  Essa conclusão — de que o desemprego é a causa natural da inflação — mostra quão equivocadas são as análises keynesianas que ficaram conhecidas como a curva de Phillips, que postulavam a existência de um trade off ou dilema entre inflação e desemprego, de modo que, se algum governo desejasse combater a inflação, teria que aceitar uma taxa de desemprego de mão de obra maior ou, se quisesse reduzir o desemprego, seria forçado a aceitar uma taxa de inflação mais elevada.

 

VII.  Conclusões

 

 

            Procuramos neste capítulo resumir a multiplicidade de fatores cujo conjunto constitui a Escola Austríaca de Economia, mostrando a importância de cada um deles na construção do edifício e também como se integram entre si.

 

            Ao núcleo seminal ou tríade básica, formada pelo conceito de ação humana, pela concepção dinâmica do tempo e pelo reconhecimento de que o conhecimento possui limitações, acrescentou-se o que se pode denominar de elementos de propagação, a saber, a doutrina da utilidade marginal, o subjetivismo e o conceito de ordens espontâneas.  É interessante para o leitor parar neste ponto e fazer o exercício de certificar-se de que cada um desses três últimos elementos decorre dos três primeiros, em maior ou menor intensidade, o que permite que sejam denominados de propagadores. 

 

            De posse desse aparato, mostramos suas implicações nos campos da filosofia política, da epistemologia e da economia. 

 

            Ação, tempo e conhecimento: eis o universo fascinante da Escola Austríaca de Economia!

            


_________________________________________________________________________
[1] Praxeologia: do grego praxis — ação, habito, pratica — e logia — doutrina, teoria, ciência. É a ciência ou
teoria geral da ação humana. Optamos pela grafia "praxeologia" no lugar de "praxiologia" para manter o
padrão utilizado pelo tradutor da obra Ação Humana para o português, Donald Stewart Jr.

