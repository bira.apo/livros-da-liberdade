## REFERÊNCIAS BIBLIOGRÁFICAS

ANTISERI, Dario. Liberi perché fallibili. Soveria Mannelli: Rubbettino Editore, 1995.

__________ . Princìpi liberali. Soveria Mannelli: Rubbettino Editore, 2003.

BÖHM-BAWERK, Eugen von. Capital and Interest: A Critical History of Economical Theory. (Translated by William A. Smart). London: Macmillan, 1890.

__________ . The Positive Theory of Capital. (Translated by William A. Smart). London: Macmillan, 1891.

CATHARINO, Alex. "Liberalismo". In: BARRETO, Vicente (Org.). Dicionário de Filosofia Política. São Leopoldo: UNISINOS, 2010. pp. 307-311.

__________ . "Origens e Desenvolvimento do Liberalismo Clássico". In: GARCIA, Aloísio Teixeira (Org.). Ensaios sobre Liberdade e Prosperidade. Belo Horizonte: UNA Editoria, 2001. pp. 57-81.

__________ . "Origens e Evolução da ciência da Riqueza e da Pobreza: Uma análise histórica da filosofia social dos economistas clássicos". In: Revista Metanóia, Número 6 (2004): 31-58.

CONSTANTINO, Rodrigo. Economia do Indivíduo: O Legado da Escola Austríaca. São Paulo: Instituto Mises Brasil, 2009.

FEIJÓ, Ricardo. Economia e Filosofia na Escola Austríaca: Menger, Mises e Hayek. São Paulo: Nobel, 2000.

GARRISON, Roger W. Time and Money: the Macroeconomics of Capital Structure. London / New York: Routledge, 2001.

GUERREIRO, Mário A. L. Ética Mínima para Homens Práticos. (Apresentação de Alberto Oliva). Rio de Janeiro: Instituto Liberal, 1995.

__________ . Igualdade ou Liberdade? Porto Alegre: EDIPUCRS, 2002.

HAYEK, Friedrich August von. Direito, Legislação e Liberdade: Uma nova formulação dos princípios liberais de Justiça e Economia Política. (Apresentação e supervisão da tradução de Henry Maksoud; tradução de Anna Maria Capovilla, José Ítalo Stelle, Manoel Paulo Ferreira e Maria Luiza X. de A. Borges). São Paulo: Editora Visão, 1985. 3v.

__________ . Individualism and Economic Order. Chicago: The University of Chicago Press, 1984.

__________ . O caminho da servidão. (Tradução e revisão de Anna Maria Capovilla, José Ítalo Stelle e Liane de Morais Ribeiro). Rio de Janeiro: Instituto Liberal, 5ª edição, 1990.

__________ . Os Fundamentos da Liberdade. (Tradução de Anna Maria Copovilla & José Ítalo Stelle). São Paulo / Brasília: Editora Visão / Editora Universidade de Brasília, 1983.

__________ . The Counter Revolution of Science: Studies on the Abuse of Reason. Indianapolis: Liberty Press, 1979.

HUERTA DE SOTO, Jesus. Socialismo, Cálculo Econômico y Función Empresarial. Madrid: Unión Editorial, 1992.

IORIO, Ubiratan J. Economia e Liberdade: A Escola Austríaca e a Economia Brasileira. (Prefácio de Roberto Campos). Rio de Janeiro: Forense Universitária, 2ª edição revista e ampliada, 1997.

KIRZNER, Israel M. The Meaning of Market Process: Essays in the development of modern Austrian economics. London / New York: Routledge, 1992.

LEME, Og Francisco. Entre os cupins e os homens. Rio de Janeiro: Instituto Liberal / José Olympio Editora, 1988.

MENGER, Carl. Principles of Economics. (Foreword by Peter G. Klein; Introduction by F. A. Hayek; Translated by James Dingwall and Bert F. Hoselitz). Auburn: Ludwig von Mises Institute, 2007.

MISES, Ludwig von. Ação humana: Um tratado de Economia. (Tradução de Donald Stewart Jr.). Rio de Janeiro: Instituto Liberal, 1990.

__________ . Epistemological Ploblems of Economics. (Translated by George Reisman; Introduction to the Third Edition by Jörg Guido Hülsmann). Auburn: Ludwig von Mises Institute, 3rd Edition, 2003.

__________ . Liberalismo: Segundo a tradição clássica. (Tradução de Haydn Coutinho Pimenta). Rio de Janeiro: José Olympio / Instituto Liberal, 1987.

__________ . Socialism: An Economic and Sociological Analysis. (Translated by J. Kahane; Foreword by F. A. Hayek). Indianapolis: Liberty Fund, 2008.

__________ . The Theory of Money and Credit. (Translated by H. E. Batson). Irvington-on-Hudson: Foundation for Economic Education, 1971.

__________ . The Ultimate Foundation of Economic Science: An Essay on Method. (Foreword by Israel M. Kirzner). Irvington-on-Hudson: Foundation for Economic Education, 2002.

MOREIRA, José Manuel. Filosofia e metodologia da economia em F. A. Hayek ou a redescoberta de um caminho "terceiro" para a compreensão e melhoria da ordem alargada da interação humana. Porto: Publicações da Universidade do Porto, 1994.

O'DRISCOLL JR., Gerald P. & RIZZO, Mario J. The Economics of Time and Ignorance. London / New York: Routledge, 1996.

OLIVA, Alberto. Conhecimento e Liberdade: Individualismo X Coletivismo. (Prefácio de Roque Spencer Maciel de Barros). Porto Alegre: EDIPUCRS, 2ª Edição revista e ampliada, 1999.

__________ . Entre o Dogmatismo Arrogante e o Desespero Cético: A Negatividade como Fundamento da Visão de Mundo Liberal. (Apresentação de Og Francisco Leme). Rio de Janeiro: Instituto Liberal, 1993.

ROTHBARD, Murray N. America's Great Depression. Kansas City: Sheed & Ward, 1972.

__________ . An Austrian Perspective on the History of Economic Thought - Volume I: Economic Though Before Adam Smith. Hants: Edward Helgar Publishing, 1995.

__________ . An Austrian Perspective on the History of Economic Thought - Volume II: Classical Economics. Hants: Edward Helgar Publishing, 1995.

__________ . Man, Economy and State: A Treatise on Economic Principles. Auburn: Ludwig von Mises Institute, 2001.

__________ . The Ethics of Liberty. New York: New York University Press, 1998.

SKOUSEN, Mark. The Making of Modern Economics: The Lives and Ideas of the Great Thinkers. Armonk: M. E. Sharpe, 2002.

__________ . Vienna & Chigago, Friends or Foes? - A Tale of Two Schools of Free-Market Economics. Washington DC: Regnery Publishing, 2005.

__________ . The Structure of Production. New York: New York University Press, 1990.

WIESER, Friedrich von. Natural Value. (Edited with a Preface and Analysis by William Smart; Translated by Christian A. Malloch). London: Macmillan, 1893.

__________ . Social Economics. (Foreword by Wesley C. Mitchell; Translated by A. F. Hinrichs). London: Routledge, 2003.

ZANOTTI, Gabriel. Epistemologia da Economia. (Tradução de Júlio Cezar R. Pereira). Porto Alegre: EDIPUCRS, 1997.