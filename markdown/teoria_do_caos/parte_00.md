---
title: Teoria do Caos
subititle: Dois ensaios sobre anarquia de mercado
author: Robert P. Murphy
trl: Erick Vasconcelos
date: 2002
language: pt-BR
cover-image: cover/teoria-do-caos.jpg
...

# Teoria do Caos

- **Autor:** Robert P. Murphy
- **Título original:** Chaos Theory
- **Ano em que foi publicado pela primeira vez:** 2002
- **Tradução:** Erick Vasconcelos

## Prefácio

Este livro é dedicado àqueles que acreditam que o governo é um mal necessário: Vocês estão metade certos.

Os seguintes ensaios mostram que as duas “funções” mais cruciais do governo — lei e defesa — podem ser eficientemente providas pelo livre-mercado. Assim, demonstra-se que o Estado é completamente desnecessário. Anarquia, a ausência de governo coercitivo, não deve ser confundida com caos.

Em vez de me envolver em debates filosóficos quanto a valores, eu enfoco a mecânica do “anarco-capitalismo” ou “anarquia de mercado”, o único sistema social que respeita a santidade dos direitos de propriedade e dos contratos. Em vez de atacar o ideal socialista de igualdade, eu ofereço o ideal anarquista de mercado de equidade.

Eu, em última instância, deixo para o leitor decidir qual sistema — Estado ou mercado — é a melhor organização.

Bob Murphy

## Agradecimentos

Meu débito intelectual em relação à teoria anarquista pertence, sem dúvida, ao grande Murray Rothbard (1926-1995). Em sentido mais amplo, minha visão de “como o mundo funciona” se deve primariamente àquela fonte de sabedoria, o tratado de Ludwig von Mises Ação Humana.

A ideia original deste livro (e a Introdução) vieram de Jeremy Sapienza. Robert Vroman proveu as maravilhosas (mesmo que meio perturbadoras) ilustrações que tão piedosamente interrompem a monotonia do texto. Matt Pramschufer fez o design da capa.

Zach Crossen e Pete Johnson revisaram um rascunho inicial deste livro. Sou grato a Lew Rockwell e Gene Callahan — dois heróis de hoje em dia na batalha das ideias — pelos gentis comentários na contracapa. Eu gostaria de agradecer aos participantes do fórum do anti-state.com por suas constantes (mesmo que às vezes ásperas) críticas e questionamentos de minhas visões certamente não-ortodoxas. Rachael Anne Fajardo teve um papel indispensável ao prover constante encorajamento e sugestões.

Finalmente, eu devo agradecer a Ron Pramschufer e o resto do grupo da RJ Comunicações por fazer parecer que eu já tinha publicado algo antes.

RPM
Abril de 2002  
